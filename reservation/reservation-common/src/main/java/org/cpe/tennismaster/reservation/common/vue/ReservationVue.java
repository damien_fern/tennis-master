package org.cpe.tennismaster.reservation.common.vue;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.cpe.tennismaster.court.common.vue.CourtVue;
import org.cpe.tennismaster.common.model.utilisateur.UtilisateurVue;

import java.time.LocalDateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReservationVue {

    private Integer idReservation;

    private UtilisateurVue idUtilisateur1;

    private UtilisateurVue idUtilisateur2;

    private CourtVue idCourt;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dateDebutReservation;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime dateCreationReservation;

    private Boolean valide;

    private String tokenReservation;

    @JsonManagedReference
    private PartieVue partie;

    public Integer getIdReservation() {
        return idReservation;
    }

    public void setIdReservation(Integer idReservation) {
        this.idReservation = idReservation;
    }

    public UtilisateurVue getIdUtilisateur1() {
        return idUtilisateur1;
    }

    public void setIdUtilisateur1(UtilisateurVue idUser) {
        this.idUtilisateur1 = idUser;
        //this.userOne.addReservationPlayer1(this);
    }

    public UtilisateurVue getIdUtilisateur2() {
        return idUtilisateur2;
    }

    public void setIdUtilisateur2(UtilisateurVue idUser) {
        this.idUtilisateur2 = idUser;
        //this.idUser2.addReservationPlayer2(this);
    }

    public CourtVue getIdCourt() {
        return idCourt;
    }

    public void setIdCourt(CourtVue idCourt) {
        this.idCourt = idCourt;
        //this.court.addReservation(this);
    }

    public LocalDateTime getDateDebutReservation() {
        return dateDebutReservation;
    }

    public void setDateDebutReservation(LocalDateTime dateDebutReservation) {
        this.dateDebutReservation = dateDebutReservation;
    }

    public LocalDateTime getDateCreationReservation() {
        return dateCreationReservation;
    }

    public void setDateCreationReservation(LocalDateTime dateCreationReservation) {
        this.dateCreationReservation = dateCreationReservation;
    }

    public Boolean getValide() {
        return valide;
    }

    public void setValide(Boolean valid) {
        valide = valid;
    }

    public String getTokenReservation() {
        return tokenReservation;
    }

    public void setTokenReservation(String tokenReservation) {
        this.tokenReservation = tokenReservation;
    }

    public PartieVue getPartie() {
        return partie;
    }

    public void setPartie(PartieVue partie) {
        this.partie = partie;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "\n\tid=" + idReservation +
                //"\n\tuserOne=" + (this.userOne == null ? "nulll" : userOne.getFirstName()) +
                //"\n\tidUser2=" + (this.idUser2 == null ? "nulll" : idUser2.getFirstName()) +
                "\n\tcourt=" + (this.idCourt == null ? "nulll" : idCourt) +
                "\n\tdateReservation=" + (this.dateDebutReservation == null ? "nulll" : dateDebutReservation) +
                "\n\tdateCreationReservation=" + (this.dateCreationReservation == null ? "nulll" : dateCreationReservation) +
                '}';
    }
}
