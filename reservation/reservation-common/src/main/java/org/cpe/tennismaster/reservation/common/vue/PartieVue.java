package org.cpe.tennismaster.reservation.common.vue;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.validation.constraints.NotNull;

public class PartieVue {

    @NotNull
    private Integer idPartie;

    @NotNull
    private String scoreJ1; //TODO: voir avec équipe pour changer format score

    @NotNull
    private String scoreJ2;

    @JsonBackReference
    private ReservationVue reservation;

    public PartieVue() {
    }

    public PartieVue(String scoreJ1, String scoreJ2) {
        this.scoreJ1 = scoreJ1;
        this.scoreJ2 = scoreJ2;
    }

    public Integer getIdPartie() {
        return idPartie;
    }

    public void setIdPartie(Integer idPartie) {
        this.idPartie = idPartie;
    }

    public String getScoreJ1() {
        return scoreJ1;
    }

    public void setScoreJ1(String scoreJ1) {
        this.scoreJ1 = scoreJ1;
    }

    public String getScoreJ2() {
        return scoreJ2;
    }

    public void setScoreJ2(String scoreJ2) {
        this.scoreJ2 = scoreJ2;
    }

    public ReservationVue getReservation() {
        return reservation;
    }

    public void setReservation(ReservationVue reservation) {
        this.reservation = reservation;
    }
}
