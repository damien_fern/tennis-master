package org.cpe.tennismaster.reservation.common.vue;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ReservationParCourtVue {

    @NotNull
    private Integer idCourt;

    @NotNull
    private List<ReservationVue> reservationVueList;


}
