package org.cpe.tennismaster.reservation.repository;


import org.cpe.tennismaster.reservation.model.Partie;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GameRepository extends JpaRepository<Partie, Integer> {

    List<Partie> findAllBy();
}
