package org.cpe.tennismaster.reservation.model;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.cpe.tennismaster.common.utils.UtilsTools;
import org.cpe.tennismaster.reservation.serialization.date.DateDeserializer;
import org.cpe.tennismaster.reservation.serialization.date.DateSerializer;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "reservation")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_reservation", updatable = false)
    private Integer idReservation;

    @NotNull
    @Column(name = "id_utilisateur_1")
    private Integer idUtilisateur1;

    @Column(name = "id_utilisateur_2")
    private Integer idUtilisateur2;

    @NotNull
    private Integer idCourt;

    @Column(name = "date_debut_reservation")
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime dateDebutReservation;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Column(name = "date_creation_reservation")
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime dateCreationReservation;


    private Boolean valide;

    @Column(name = "token_reservation", unique = true, length = 16, columnDefinition = "bpchar")
    @NotNull
    private String tokenReservation;

    @OneToOne(mappedBy = "reservation", cascade = CascadeType.REMOVE)
    @JsonManagedReference
    private Partie partie;

    @PreUpdate
    void preUpdate() {
        this.dateCreationReservation = LocalDateTime.now();
        if(valide == null) { //TODO: à changer une fois que regles_club créée
            valide = true;
        }
    }

    @PrePersist
    void prePersist() {
        if(this.tokenReservation == null || tokenReservation.isEmpty()) {
            this.tokenReservation = UtilsTools.generateString(16);
        }
        this.preUpdate();
    }

    public Reservation() {
    }

    public Reservation(Integer idUtilisateur1, Integer idUtilisateur2, Integer idCourt, LocalDateTime dateDebutReservation, LocalDateTime dateCreationReservation, Boolean valide, Partie partie) {
        this.idUtilisateur1 = idUtilisateur1;
        this.idUtilisateur2 = idUtilisateur2;
        this.idCourt = idCourt;
        this.dateDebutReservation = dateDebutReservation;
        this.dateCreationReservation = dateCreationReservation;
        this.valide = valide;
        this.partie = partie;
    }

    public Integer getIdReservation() {
        return idReservation;
    }

    public void setIdReservation(Integer idReservation) {
        this.idReservation = idReservation;
    }

    public Integer getIdUtilisateur1() {
        return idUtilisateur1;
    }

    public void setIdUtilisateur1(Integer idUser) {
        this.idUtilisateur1 = idUser;
        //this.userOne.addReservationPlayer1(this);
    }

    public Integer getIdUtilisateur2() {
        return idUtilisateur2;
    }

    public void setIdUtilisateur2(Integer idUser) {
        this.idUtilisateur2 = idUser;
        //this.idUser2.addReservationPlayer2(this);
    }

    public Integer getIdCourt() {
        return idCourt;
    }

    public void setIdCourt(Integer idCourt) {
        this.idCourt = idCourt;
        //this.court.addReservation(this);
    }

    public LocalDateTime getDateDebutReservation() {
        return dateDebutReservation;
    }

    public void setDateDebutReservation(LocalDateTime dateDebutReservation) {
        this.dateDebutReservation = dateDebutReservation;
    }

    public LocalDateTime getDateCreationReservation() {
        return dateCreationReservation;
    }

    public void setDateCreationReservation(LocalDateTime dateCreationReservation) {
        this.dateCreationReservation = dateCreationReservation;
    }

    public Boolean getValide() {
        return valide;
    }

    public void setValide(Boolean valid) {
        valide = valid;
    }

    public String getTokenReservation() {
        return tokenReservation;
    }

    public void setTokenReservation(String tokenReservation) {
        this.tokenReservation = tokenReservation;
    }

    public Partie getPartie() {
        return partie;
    }

    public void setPartie(Partie partie) {
        this.partie = partie;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "\n\tid=" + idReservation +
                //"\n\tuserOne=" + (this.userOne == null ? "nulll" : userOne.getFirstName()) +
                //"\n\tidUser2=" + (this.idUser2 == null ? "nulll" : idUser2.getFirstName()) +
                "\n\tcourt=" + (this.idCourt == null ? "nulll" : idCourt) +
                "\n\tdateReservation=" + (this.dateDebutReservation == null ? "nulll" : dateDebutReservation) +
                "\n\tdateCreationReservation=" + (this.dateCreationReservation == null ? "nulll" : dateCreationReservation) +
                '}';
    }
}
