package org.cpe.tennismaster.reservation.service;

import org.cpe.tennismaster.common.exception.InternalServerException;
import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.exception.ParameterException;
import org.cpe.tennismaster.common.model.utilisateur.UtilisateurVue;
import org.cpe.tennismaster.reservation.common.dto.ReservationDto;
import org.cpe.tennismaster.reservation.common.exception.NoReservationPossibleException;
import org.cpe.tennismaster.reservation.common.exception.UserCourtNotExistException;
import org.cpe.tennismaster.reservation.common.vue.ReservationGrantedVue;
import org.cpe.tennismaster.reservation.common.vue.ReservationParCourtVue;
import org.cpe.tennismaster.reservation.common.vue.ReservationVue;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public interface ReservationService {

    List<ReservationVue> getAllReservations();

    ReservationGrantedVue tokenReservationAutorise(String code);

    List<ReservationVue> getReservationsByWeek(String dateString) throws InternalServerException;

    List<ReservationVue> getReservationsByWeek(Integer id) throws InternalServerException;

    ReservationVue getReservationById(Integer id) throws NotFoundException;

    List<ReservationParCourtVue> getPlanningReservation(LocalDate dateReservation, Integer idTypeCourt, Boolean estCouvert, String libelleCourt, Integer idCourt) throws ParameterException;

    List<ReservationParCourtVue> getDispoReservation(LocalDate dateReservation, Integer idTypeCourt, Boolean estCouvert, String libelleCourt, Integer idCourt) throws ParameterException;

    List<ReservationParCourtVue> getReservationByCourt(LocalDate dateReservation, Integer idTypeCourt, Boolean estCouvert, String libelleCourt, Integer idCourt) throws ParameterException;

    ReservationVue createReservation(ReservationDto reservationDto) throws ParameterException, NoReservationPossibleException;

    ReservationVue createReservationByUser(ReservationDto reservationDto, UtilisateurVue utilisateurVue) throws ParameterException, NoReservationPossibleException;

    Void removeReservation(Integer id) throws NotFoundException;

    ReservationVue updateReservation(Integer id, ReservationDto reservationDto) throws ParameterException, UserCourtNotExistException, NotFoundException;

    List<ReservationVue> getReservationByUser(UtilisateurVue utilisateurVue) throws InternalServerException;

    Void removeReservationByUser(Integer id, UtilisateurVue utilisateurVue);

    List<ReservationVue> getReservationByUserId(Integer utilisateurId);
}
