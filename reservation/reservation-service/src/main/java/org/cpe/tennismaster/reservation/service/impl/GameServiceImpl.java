package org.cpe.tennismaster.reservation.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.exception.ParameterException;
import org.cpe.tennismaster.common.utils.UtilsTools;
import org.cpe.tennismaster.reservation.common.dto.PartieDto;
import org.cpe.tennismaster.reservation.common.vue.PartieVue;
import org.cpe.tennismaster.reservation.model.Partie;
import org.cpe.tennismaster.reservation.repository.GameRepository;
import org.cpe.tennismaster.reservation.service.GameService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class GameServiceImpl implements GameService {

    private final GameRepository gameRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    public GameServiceImpl(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public List<PartieVue> getAllGame() {
        List<Partie> partieList =  this.gameRepository.findAllBy();
        Type listType = new TypeToken<List<PartieVue>>() {}.getType();
        return modelMapper.map(partieList, listType);
    }


    public PartieVue getGameById(Integer id) throws NotFoundException {
        Optional<Partie> optPartie = gameRepository.findById(id);
        if (!optPartie.isPresent()) {
            throw new NotFoundException("Partie " + id);
        }
        return  modelMapper.map(optPartie.get(), PartieVue.class);
    }

    public PartieVue createGame(PartieDto partieDto) throws ParameterException {
        if (partieDto == null) {
            throw new ParameterException();
        }
        Partie partieModel = modelMapper.map(partieDto, Partie.class);
        partieModel = gameRepository.save(partieModel);

        return modelMapper.map(partieModel, PartieVue.class);
    }

    public Void removeGame(Integer id) throws NotFoundException {
        Optional<Partie> optGame = gameRepository.findById(id);
        if (!optGame.isPresent()) {
            throw new NotFoundException("Partie " + id);
        }
        gameRepository.delete(optGame.get());
        return null;
    }

    public PartieVue updateGame(Integer id, PartieDto partieDto) throws ParameterException {
        if (partieDto == null) {
            throw new ParameterException();
        }

        Optional<Partie> optExistingPartie = gameRepository.findById(id);
        if (!optExistingPartie.isPresent()) {
            throw new NotFoundException("Partie " + id);
        }

        Partie partieModel = modelMapper.map(partieDto, Partie.class);
        UtilsTools.copyNonNullProperties(partieModel, optExistingPartie.get());
        partieModel = gameRepository.save(optExistingPartie.get());

        return modelMapper.map(partieModel, PartieVue.class);
    }
}
