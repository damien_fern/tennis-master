package org.cpe.tennismaster.common.model.utilisateur;

import java.util.Arrays;

public enum Gender {
    H('H'), F('F'), U('U');

    char name;

    Gender(char name) {
        this.name = name;
    }

    public char getName() {
        return name;
    }

    public static Gender getByName(char name) {
        return Arrays.stream(Gender.values())
                .filter(gender -> gender.getName() == name)
                .findFirst()
                .orElse(U);
    }
}
