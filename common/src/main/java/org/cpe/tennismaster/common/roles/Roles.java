package org.cpe.tennismaster.common.roles;

public enum Roles {
    USER(Libelle.USER), ADMIN(Libelle.ADMIN);

    private String libelleRole;

    Roles(String libelleRole) {
        this.libelleRole = libelleRole;
    }

    public String getLibelle() {
        return this.libelleRole;
    }

    public static class Libelle {

        public  final static String USER = "ROLE_USER";
        public  final static String ADMIN = "ROLE_ADMIN";
    }
}
