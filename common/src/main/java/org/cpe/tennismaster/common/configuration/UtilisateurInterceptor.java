package org.cpe.tennismaster.common.configuration;

import org.cpe.tennismaster.common.exception.UnauthorizedException;
import org.cpe.tennismaster.common.model.utilisateur.UtilisateurVue;
import org.cpe.tennismaster.common.utils.UtilsTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class UtilisateurInterceptor extends HandlerInterceptorAdapter {

    private final UtilsTools utilsTools;

    private final String baseUrlUtilisateurGet;

    private final String serviceName;

    public UtilisateurInterceptor(UtilsTools utilsTools, String baseUrlUtilisateurGet, String serviceName) {
        this.utilsTools = utilsTools;
        this.baseUrlUtilisateurGet = baseUrlUtilisateurGet;
        this.serviceName = serviceName;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if(isUserLogged() && !serviceName.equals("user-service")) {
            this.addUtilisateurToCurrentRequest(request);
        }
        return true;
    }

    private static boolean isUserLogged() {
        try {
            return !SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser");
        } catch (Exception e) {
            return false;
        }
    }

    private void addUtilisateurToCurrentRequest(HttpServletRequest httpServletRequest) {
        UtilisateurVue utilisateurVue = utilsTools.restCall(baseUrlUtilisateurGet + "me", HttpMethod.GET, new ParameterizedTypeReference<UtilisateurVue>() {});

        //Check if users and court exist
        if (utilisateurVue == null) {
            throw new UnauthorizedException();
        }

        httpServletRequest.setAttribute("user", utilisateurVue);
    }

}
