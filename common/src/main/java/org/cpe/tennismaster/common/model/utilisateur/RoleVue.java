package org.cpe.tennismaster.common.model.utilisateur;

import javax.validation.constraints.NotNull;

public class RoleVue {

    @NotNull
    private Integer idRole;

    @NotNull
    private String libelleRole;

    @NotNull
    private String libelleRoleAuth;

    public RoleVue() {
    }

    public RoleVue(String libelleRole, String libelleRoleAuth) {
        this.libelleRole = libelleRole;
        this.libelleRoleAuth = libelleRoleAuth;
    }

    public Integer getIdRole() {
            return idRole;
        }

    public void setIdRole(Integer idRole) {
            this.idRole = idRole;
        }

    public String getLibelleRole() {
            return libelleRole;
        }

    public void setLibelleRole(String libelleRole) {
            this.libelleRole = libelleRole;
        }

    public String getLibelleRoleAuth() {
        return libelleRoleAuth;
    }

    public void setLibelleRoleAuth(String libelleRoleAuth) {
        this.libelleRoleAuth = libelleRoleAuth;
    }
}
