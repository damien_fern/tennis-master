package org.cpe.tennismaster.common.configuration;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.DefaultResponseErrorHandler;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;

@Slf4j
public class RestTemplateErrorHandler extends DefaultResponseErrorHandler {

    private static final Logger logger = LoggerFactory.getLogger(RestTemplateErrorHandler.class);

    @Override
    public void handleError(URI url, HttpMethod method, ClientHttpResponse response) throws IOException {
        logger.error("============================response begin==========================================");
        logger.error("Status code  : {}", response.getStatusCode());
        logger.error("Status text  : {}", response.getStatusText());
        logger.error("Method  : {}", method.name());
        logger.error("URL  : {}", url.getHost() + ":" + url.getPort() + url.getPath());
        logger.error("Headers      : {}", response.getHeaders());
        logger.error("Response body: {}", StreamUtils.copyToString(response.getBody(), Charset.defaultCharset()));
        logger.error("=======================response end=================================================");
    }
}
