package org.cpe.tennismaster.common.configuration.activeMQ;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.cpe.tennismaster.common.model.Letter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;

@Slf4j
public abstract class GeneralActiveMQOperation {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Qualifier("objectMapper")
    @Autowired
    protected ObjectMapper mapper;

    @JmsListener(destination = "${queue.destination}", containerFactory = "connectionFactory")
    private void onMessage(Letter letter){
        log.info("New ActiveMQ message has arrived : {}", letter.toString());
        this.receiveMessage(letter);
    }

    protected abstract void receiveMessage(Letter letter);

    public void sendMessage(Letter letter) {
        try {
            if(letter == null || letter.getToQueue() == null){
                    throw new Exception("No Queue set");
            }
            jmsTemplate.convertAndSend(letter.getToQueue().getName(), letter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
