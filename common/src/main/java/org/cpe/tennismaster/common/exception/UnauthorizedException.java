package org.cpe.tennismaster.common.exception;

import org.springframework.http.HttpStatus;

public class UnauthorizedException extends GeneralException {

    public UnauthorizedException(){
        super();
    }

    @Override
    protected HttpStatus defineStatusHttp() {
        return HttpStatus.UNAUTHORIZED;
    }

    @Override
    protected String defineErrorMessage(String variable) {
        return "Not authorized.";
    }
}
