package org.cpe.tennismaster.common.model.utilisateur;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.NotNull;
import java.util.Set;

public class ClassementVue {

    @NotNull
    private Long idClassement;

    @NotNull
    private String libelleClassement;

    public ClassementVue() {

    }

    public ClassementVue(String libelleClassement) {
        this.libelleClassement = libelleClassement;
    }

    public Long getIdClassement() {
        return idClassement;
    }

    public void setIdClassement(Long idClassement) {
        this.idClassement = idClassement;
    }

    public String getLibelleClassement() {
        return libelleClassement;
    }

    public void setLibelleClassement(String libelleClassement) {
        this.libelleClassement = libelleClassement;
    }
}
