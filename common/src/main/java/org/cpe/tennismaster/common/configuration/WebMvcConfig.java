package org.cpe.tennismaster.common.configuration;

import org.cpe.tennismaster.common.utils.UtilsTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Value(value = "http://${service-utilisateur.ip}"+":${service-utilisateur.port}"+"/${service-utilisateur.get}/")
    private String baseUrlUtilisateurGet;

    @Value(value = "${spring.application.name}")
    private String serviceName;

    private final UtilsTools utilsTools;

    @Autowired
    public WebMvcConfig(UtilsTools utilsTools) {
        this.utilsTools = utilsTools;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new UtilisateurInterceptor(utilsTools, baseUrlUtilisateurGet, serviceName));
    }

}
