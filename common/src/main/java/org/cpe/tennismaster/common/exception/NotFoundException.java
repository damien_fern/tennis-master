package org.cpe.tennismaster.common.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends GeneralException {

    public NotFoundException(String attribut){
        super(attribut);
    }

    @Override
    protected HttpStatus defineStatusHttp() {
        return HttpStatus.NOT_FOUND;
    }

    @Override
    protected String defineErrorMessage(String variable) {
        return variable + " n'a pas été trouvé(e).";
    }
}
