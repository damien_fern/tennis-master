package org.cpe.tennismaster.common.model;

public enum Intent {
    SEND_MAIL_NOTIFICATION, SEND_PUSH_NOTIFICATION
}
