package org.cpe.tennismaster.common.exception;

import org.springframework.http.HttpStatus;

public class InternalServerException extends GeneralException {

    public InternalServerException(){
        super();
    }

    @Override
    protected HttpStatus defineStatusHttp() {
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

    @Override
    protected String defineErrorMessage(String variable) {
        return "Une erreur interne au serveur a été rencontrée.";
    }
}
