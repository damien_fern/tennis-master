package org.cpe.tennismaster.common.exception;

import org.cpe.tennismaster.common.model.ResponseView;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GeneralExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { Exception.class })
    protected ResponseEntity<ResponseView<Object>> handleConflict(RuntimeException ex, WebRequest request) {
        int httpCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
        String message = ex.getMessage();

        if(ex instanceof GeneralException){
            httpCode = ((GeneralException) ex).getCodeHttp();
            message = ((GeneralException) ex).getMessage();
        }

        return ResponseEntity.status(httpCode).body(new ResponseView<>(httpCode, message, null));
    }

}
