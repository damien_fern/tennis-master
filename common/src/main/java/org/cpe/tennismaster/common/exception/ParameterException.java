package org.cpe.tennismaster.common.exception;

import org.springframework.http.HttpStatus;

public class ParameterException extends GeneralException {

    public ParameterException(){
        super();
    }

    @Override
    protected HttpStatus defineStatusHttp() {
        return HttpStatus.BAD_REQUEST;
    }

    @Override
    protected String defineErrorMessage(String variable) {
        return "Les paramètres ne sont pas corrects.";
    }
}
