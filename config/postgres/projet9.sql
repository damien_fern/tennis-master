SET TIME ZONE 'CET';
SELECT pg_reload_conf();

create table role
(
	id_role serial not null
		constraint role_pk
			primary key,
	libelle_role text not null,
	libelle_role_auth text not null
);

create unique index roles_id_role_uindex
    on role (id_role);

create unique index roles_libelle_role_uindex
	on role (libelle_role);

create unique index roles_libelle_auth_role_uindex
	on role (libelle_role_auth);

create table classement
(
	id_classement serial not null
		constraint classements_pk
			primary key,
	libelle_classement text not null
);

create unique index classements_id_classement_uindex
	on classement (id_classement);

create table type_court
(
	id_type_court serial not null
		constraint type_court_pk
			primary key,
	libelle_type_court varchar(50) not null
);

create unique index type_court_id_type_court_uindex
	on type_court (id_type_court);

create table court
(
	id_court serial not null
		constraint courts_pk
			primary key,
	libelle_court text not null,
	localisation_court text,
	latitude_court numeric not null,
	longitude_court numeric not null,
	couvert boolean not null,
	reservable boolean not null,
	id_type_court integer not null
		constraint courts_type_court_id_type_court_fk
			references type_court
				on update cascade on delete set null
);

create unique index courts_id_court_uindex
	on court (id_court);

create table utilisateur
(
	id_utilisateur serial not null
		constraint utilisateurs_pk
			primary key,
	nom_utilisateur varchar(20) not null,
	prenom_utilisateur varchar(20) not null,
	date_naissance_utilisateur date,
	date_inscription_utilisateur date,
	date_activation_connexion_utilisateur date,
	date_derniere_connexion_utilisateur date,
	email_utilisateur varchar(100) not null,
	login_utilisateur varchar(20) not null,
	mot_passe_utilisateur text not null,
	adresse_1_utilisateur text not null,
	adresse_2_utilisateur text,
	cp_utilisateur varchar(5) not null,
	ville_utilisateur text not null,
	genre_utilisateur char not null,
	presence_annuaire boolean default true not null,
	token_phone_utilisateur text,
	est_active_utilisateur boolean default false not null,
	id_role integer
		constraint utilisateurs_roles_id_role_fk
			references role
				on update cascade on delete set null,
	id_classement integer
		constraint utilisateurs_classements_id_classement_fk
			references classement
				on update cascade on delete set null,
	credit_invitation integer
);

comment on column utilisateur.credit_invitation is 'le nb d''invitation restant pour chaque joueur';

create unique index utilisateurs_id_utilisateur_uindex
	on utilisateur (id_utilisateur);

create table reservation
(
	id_reservation serial not null
		constraint reservations_pk
			primary key,
	date_debut_reservation timestamp not null,
	date_creation_reservation timestamp not null,
	valide boolean default true not null,
	token_reservation char(16) not null,
	id_court integer
		constraint reservations_courts_id_court_fk
			references court
				on update cascade on delete restrict,
	id_utilisateur_1 integer
		constraint reservations_utilisateurs_id_utilisateur_fk
			references utilisateur
				on update cascade on delete restrict,
	id_utilisateur_2 integer
		constraint reservations_utilisateurs_id_utilisateur_fk_2
			references utilisateur
				on update cascade on delete restrict
);

comment on table reservation is 'durée fixe = 1h';

create unique index reservations_id_reservation_uindex
	on reservation (id_reservation);

create unique index reservations_token_uindex
	on reservation (token_reservation);

create table partie
(
	id_partie serial not null
		constraint parties_pk
			primary key,
	score_j_1 text not null,
	score_j_2 text not null,
	id_reservation integer
		constraint parties_reservations_id_reservation_fk
			references reservation
				on update cascade on delete restrict
);

create unique index parties_id_partie_uindex
	on partie (id_partie);

create table news
(
	id_news serial not null
		constraint news_pk
			primary key,
	titre_news varchar(100) not null,
	contenu_news text not null,
	date_creation_news timestamp not null,
	image_url text
);

create unique index news_id_news_uindex
	on news (id_news);

create table regles_club
(
	id_regle serial not null
		constraint regles_club_pk
			primary key,
	nombre_invitation integer default 6 not null,
	horaire_debut_acces time not null,
	horaire_fin_ouverture time not null,
	nb_jour_max_reservation integer not null,
	libelle_regle varchar(50)
);

comment on column regles_club.nb_jour_max_reservation is 'nombre de jour reservable par client et par mois';

create unique index regles_club_id_regle_uindex
	on regles_club (id_regle);

create table type_produit
(
	id_type_produit serial not null
		constraint types_produits_pk
			primary key,
	libelle_type_produit varchar(50) not null
);

create table produit
(
	id_produit serial not null
		constraint produits_pk
			primary key,
	libelle_produit varchar(50) not null,
	prix_produit numeric not null,
	description_produit text not null,
	image_url text not null,
	id_type_produit integer not null
		constraint "produitstypes_produits_id_type_produit_fk"
			references type_produit
				on update cascade on delete set null
);

create table commande
(
	id_commande serial not null
		constraint commandes_pk
			primary key,
	date_commande date not null,
	prix_commande numeric not null,
	id_utilisateur integer not null
		constraint commandes_utilisateurs_id_utilisateur_fk
			references utilisateur
				on update cascade on delete restrict,
	id_produit integer not null
		constraint commandes_produits_id_produit_fk
			references produit
				on update cascade on delete restrict
);

create unique index commandes_id_commande_uindex
	on commande (id_commande);

create unique index produits_id_produits_uindex
	on produit (id_produit);

create unique index types_produit_id_type_produit_uindex
	on type_produit (id_type_produit);

