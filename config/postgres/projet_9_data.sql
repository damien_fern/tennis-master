INSERT INTO classement (id_classement, libelle_classement) VALUES (1, '40');
INSERT INTO classement (id_classement, libelle_classement) VALUES (2, '30/5');
INSERT INTO classement (id_classement, libelle_classement) VALUES (3, '30/4');
INSERT INTO classement (id_classement, libelle_classement) VALUES (4, '30/3');
INSERT INTO classement (id_classement, libelle_classement) VALUES (5, '30/2');
INSERT INTO classement (id_classement, libelle_classement) VALUES (6, '30/1');
INSERT INTO classement (id_classement, libelle_classement) VALUES (7, '30');
INSERT INTO classement (id_classement, libelle_classement) VALUES (8, '15/5');
INSERT INTO classement (id_classement, libelle_classement) VALUES (9, '15/4');
INSERT INTO classement (id_classement, libelle_classement) VALUES (10, '15/3');
INSERT INTO classement (id_classement, libelle_classement) VALUES (11, '15/2');
INSERT INTO classement (id_classement, libelle_classement) VALUES (12, '15/1');
INSERT INTO classement (id_classement, libelle_classement) VALUES (13, '15');
INSERT INTO classement (id_classement, libelle_classement) VALUES (14, '5/6');
INSERT INTO classement (id_classement, libelle_classement) VALUES (15, '4/6');
INSERT INTO classement (id_classement, libelle_classement) VALUES (16, '3/6');
INSERT INTO classement (id_classement, libelle_classement) VALUES (17, '2/6');
INSERT INTO classement (id_classement, libelle_classement) VALUES (18, '1/6');
INSERT INTO classement (id_classement, libelle_classement) VALUES (19, '0');
INSERT INTO classement (id_classement, libelle_classement) VALUES (20, '-2/6');
INSERT INTO classement (id_classement, libelle_classement) VALUES (21, '-4/6');
INSERT INTO classement (id_classement, libelle_classement) VALUES (22, '-15');
INSERT INTO classement (id_classement, libelle_classement) VALUES (23, '-30');
alter sequence classement_id_classement_seq RESTART WITH 24;

INSERT INTO role (id_role, libelle_role, libelle_role_auth) VALUES (1, 'utilisateur', 'ROLE_USER');
INSERT INTO role (id_role, libelle_role, libelle_role_auth) VALUES (2, 'administrateur', 'ROLE_ADMIN');
alter sequence role_id_role_seq RESTART WITH 3;

INSERT INTO utilisateur (id_utilisateur, nom_utilisateur, prenom_utilisateur, date_naissance_utilisateur, date_inscription_utilisateur,email_utilisateur, login_utilisateur, mot_passe_utilisateur, adresse_1_utilisateur, adresse_2_utilisateur, cp_utilisateur, ville_utilisateur, genre_utilisateur, presence_annuaire, id_role, id_classement, credit_invitation) VALUES (1, 'LAHOUZE', 'Benjamin', '1997-06-17', '2020-01-07','thehoyr@gmail.com', 'thehoyr', '{bcrypt}$2a$10$MlzmsHFI9tmwGgaB3OBwXOSOOyOGT9mK3loo5dme5Af8zjmH6x/KC', '35 Rue des Alpins', null, '74000', 'Annecy', 'H', true, 2, 13, 6);
INSERT INTO utilisateur (id_utilisateur, nom_utilisateur, prenom_utilisateur, date_naissance_utilisateur, date_inscription_utilisateur,email_utilisateur, login_utilisateur, mot_passe_utilisateur, adresse_1_utilisateur, adresse_2_utilisateur, cp_utilisateur, ville_utilisateur, genre_utilisateur, presence_annuaire, id_role, id_classement, credit_invitation) VALUES (2, 'GUERNON', 'Orville ', '1984-04-17', '2020-01-07', 'OrvilleGuernon@armyspy.com', 'OrvilleGuernon', '{bcrypt}$2a$10$MlzmsHFI9tmwGgaB3OBwXOSOOyOGT9mK3loo5dme5Af8zjmH6x/KC', '53, Avenue De Marlioz', null, '95100', 'Argenteuil', 'F', true, 1, 20, 5);
INSERT INTO utilisateur (id_utilisateur, nom_utilisateur, prenom_utilisateur, date_naissance_utilisateur, date_inscription_utilisateur,email_utilisateur, login_utilisateur, mot_passe_utilisateur, adresse_1_utilisateur, adresse_2_utilisateur, cp_utilisateur, ville_utilisateur, genre_utilisateur, presence_annuaire, id_role, id_classement, credit_invitation) VALUES (3, 'POULIN', 'René', '1994-11-21', '2020-01-07', 'RenePoulin@rhyta.com', 'RenePoulin', '{bcrypt}$2a$10$MlzmsHFI9tmwGgaB3OBwXOSOOyOGT9mK3loo5dme5Af8zjmH6x/KC', '38, rue Beauvau', null, '13002', 'Marseille', 'H', true, 1, 10, 6);
INSERT INTO utilisateur (id_utilisateur, nom_utilisateur, prenom_utilisateur, date_naissance_utilisateur, date_inscription_utilisateur,email_utilisateur, login_utilisateur, mot_passe_utilisateur, adresse_1_utilisateur, adresse_2_utilisateur, cp_utilisateur, ville_utilisateur, genre_utilisateur, presence_annuaire, id_role, id_classement, credit_invitation) VALUES (4, 'PELLETIER', 'Satordi', '1997-08-17', '2020-01-07', 'SatordiPelletier@jourrapide.com', 'Thavest', '{bcrypt}$2a$10$MlzmsHFI9tmwGgaB3OBwXOSOOyOGT9mK3loo5dme5Af8zjmH6x/KC', '51, cours Jean Jaures', '', '33200', 'Bordeaux', 'H', true, 1, 12, 4);
INSERT INTO utilisateur (id_utilisateur, nom_utilisateur, prenom_utilisateur, date_naissance_utilisateur, date_inscription_utilisateur,email_utilisateur, login_utilisateur, mot_passe_utilisateur, adresse_1_utilisateur, adresse_2_utilisateur, cp_utilisateur, ville_utilisateur, genre_utilisateur, presence_annuaire, id_role, id_classement, credit_invitation) VALUES (5, 'BOURDETTE', 'Dorene', '1970-09-07', '2020-01-07', 'DoreneBourdette@armyspy.com', 'Oloyed', '{bcrypt}$2a$10$MlzmsHFI9tmwGgaB3OBwXOSOOyOGT9mK3loo5dme5Af8zjmH6x/KC', '65, Cours Marechal-Joffre', '', '59220', 'DENAIN', 'F', true, 1, 3, 3);
INSERT INTO utilisateur (id_utilisateur, nom_utilisateur, prenom_utilisateur, date_naissance_utilisateur, date_inscription_utilisateur,email_utilisateur, login_utilisateur, mot_passe_utilisateur, adresse_1_utilisateur, adresse_2_utilisateur, cp_utilisateur, ville_utilisateur, genre_utilisateur, presence_annuaire, id_role, id_classement, credit_invitation) VALUES (6, 'JOSSEAUME', 'Campbell ', '1989-01-23', '2020-01-07', 'CampbellJosseaume@rhyta.com', 'Fics1989', '{bcrypt}$2a$10$MlzmsHFI9tmwGgaB3OBwXOSOOyOGT9mK3loo5dme5Af8zjmH6x/KC', '37, rue Porte d''Orange', null, '97300', 'Cayenne', 'H', true, 1, 20, 5);
INSERT INTO public.utilisateur (id_utilisateur, nom_utilisateur, prenom_utilisateur, date_naissance_utilisateur, date_inscription_utilisateur, date_activation_connexion_utilisateur, date_derniere_connexion_utilisateur, email_utilisateur, login_utilisateur, mot_passe_utilisateur, adresse_1_utilisateur, adresse_2_utilisateur, cp_utilisateur, ville_utilisateur, genre_utilisateur, presence_annuaire, token_phone_utilisateur, est_active_utilisateur, id_role, id_classement, credit_invitation) VALUES (8, 'FERNANDES', 'Damien', '1996-05-27', '2020-01-20', null, '2020-01-20', 'damien.fernandes24@gmail.com', 'dams', '{bcrypt}$2a$10$MlzmsHFI9tmwGgaB3OBwXOSOOyOGT9mK3loo5dme5Af8zjmH6x/KC', 'test adresse 1', 'test adresse 2', '69001', 'Lyon', 'H', true, null, true, 2, 2, null);
alter sequence utilisateur_id_utilisateur_seq RESTART WITH 8;

INSERT INTO "type_produit" (id_type_produit, libelle_type_produit) VALUES (1, 'Achat');
INSERT INTO "type_produit" (id_type_produit, libelle_type_produit) VALUES (2, 'Location');
alter sequence type_produit_id_type_produit_seq RESTART WITH 3;

INSERT INTO produit (id_produit, libelle_produit, prix_produit, description_produit, image_url, id_type_produit) VALUES (1, 'Balle', 2, 'Une balle de tennis pour jouer comme un pro', 'https://www.mathcurve.com/courbes3d/couture/balletennispovray.jpg', 1);
INSERT INTO produit (id_produit, libelle_produit, prix_produit, description_produit, image_url, id_type_produit) VALUES (2, 'Raquette', 20, 'Une raquette de tennis pour gagner tous vos matches', 'https://contents.mediadecathlon.com/p1250137/2000x2000/sq/raquette_de_tennis_adulte_tr_900_noir_orange_artengo_8488907_1250137.jpg?k=42c9d8c61ef05468427e0b4ce9548647', 2);
alter sequence produit_id_produit_seq RESTART WITH 3;

-- INSERT INTO commande (id_commande, date_commande, prix_commande, id_utilisateur, id_produit) VALUES (1, '2019-12-01', 20, 2, 1);
-- INSERT INTO commande (id_commande, date_commande, prix_commande, id_utilisateur, id_produit) VALUES (2, '2019-12-01', 20, 5, 1);
-- INSERT INTO commande (id_commande, date_commande, prix_commande, id_utilisateur, id_produit) VALUES (3, '2019-12-01', 2, 2, 2);
-- alter sequence commande_id_commande_seq RESTART WITH 4;

INSERT INTO type_court (id_type_court, libelle_type_court) VALUES (1, 'Acrylique');
INSERT INTO type_court (id_type_court, libelle_type_court) VALUES (2, 'Asphalte');
INSERT INTO type_court (id_type_court, libelle_type_court) VALUES (3, 'Béton');
INSERT INTO type_court (id_type_court, libelle_type_court) VALUES (4, 'Gazon');
INSERT INTO type_court (id_type_court, libelle_type_court) VALUES (5, 'Gazon synthétique');
INSERT INTO type_court (id_type_court, libelle_type_court) VALUES (6, 'Moquette');
INSERT INTO type_court (id_type_court, libelle_type_court) VALUES (7, 'Terre battue');
INSERT INTO type_court (id_type_court, libelle_type_court) VALUES (8, 'Terre artificielle');
INSERT INTO type_court (id_type_court, libelle_type_court) VALUES (9, 'parquet');
alter sequence type_court_id_type_court_seq RESTART WITH 10;

INSERT INTO court (id_court, libelle_court, localisation_court, latitude_court, longitude_court, couvert, reservable, id_type_court) VALUES (1, 'Saez tennis', 'Stade Saez', 45.734549, 4.883644, false, true, 5);
INSERT INTO court (id_court, libelle_court, localisation_court, latitude_court, longitude_court, couvert, reservable, id_type_court) VALUES (2, 'La Bulle', 'RHODIA tennis', 45.764787, 4.797261, true, true, 3);
INSERT INTO court (id_court, libelle_court, localisation_court, latitude_court, longitude_court, couvert, reservable, id_type_court) VALUES (3, 'Descartes ens tennis', 'court de tennis ENS', 45.729753, 4.826785, false, true, 3);
alter sequence court_id_court_seq RESTART WITH 4;


INSERT INTO news (id_news, titre_news, contenu_news, date_creation_news, image_url) VALUES (1, 'Ouverture du club', 'Aujourd''hui, nous vous annonçons officiellement l''ouverture de notre club prochainement.

Il s''agira d''un complexe flambant neuf avec plein de nouveaux équipement et une gestion des réservations automatisée.

Revenez sur notre site régulièrement pour être au courant.', '2020-01-23 13:13:56.145000', 'http://www.arbp.fr/images/com_allevents/vignettes/vignette_fte.jpg');
alter sequence news_id_news_seq RESTART WITH 2;

INSERT INTO reservation (id_reservation, date_debut_reservation, date_creation_reservation, valide, token_reservation, id_court, id_utilisateur_1, id_utilisateur_2) VALUES (1, '2019-12-01 10:00:00', '2019-11-29 10:00:00', true, 'aaaaaaaaaaaaaaaa', 3, 2, 5);
INSERT INTO reservation (id_reservation, date_debut_reservation, date_creation_reservation, valide, token_reservation, id_court, id_utilisateur_1, id_utilisateur_2) VALUES (2, '2019-08-15 11:00:00', '2019-07-31 11:00:00', true, 'aaaaaaaaaaaaaaab', 2, 3, 6);
alter sequence reservation_id_reservation_seq RESTART WITH 3;

INSERT INTO partie (id_partie, score_j_1, score_j_2, id_reservation) VALUES (1, '6/4 6/3', '4/6 3/6', 1);
INSERT INTO partie (id_partie, score_j_1, score_j_2, id_reservation) VALUES (2, '6/4 5/6 3/6', '4/6 6/5 6/3', 2);
alter sequence partie_id_partie_seq RESTART WITH 3;

INSERT INTO regles_club (id_regle, nombre_invitation, horaire_debut_acces, horaire_fin_ouverture, nb_jour_max_reservation, libelle_regle) VALUES (1, 6, '08:00:00', '18:00:00', 7, 'default');
alter sequence regles_club_id_regle_seq RESTART WITH 2;













