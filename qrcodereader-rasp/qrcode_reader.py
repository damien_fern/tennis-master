# qrcode_reader.py

import requests
import pygame
import os

# api-endpoint
apiURL = os.environ['API_URL']
URL = "http://" + apiURL + "/validateToken"

pygame.mixer.init()
pygame.mixer.music.load("doorOpened.mp3")

while 1:
    newCode = input('Waiting for code\n')
    PARAMS = {'code': newCode}
    r = requests.get(url=URL, params=PARAMS)
    responseJSON = r.json()
    codeSuccessBoolean = responseJSON['granted']

    if codeSuccessBoolean is True:
        print("La porte est ouverte !")
        pygame.mixer.music.play()
        while pygame.mixer.music.get_busy():
            continue
    else:
        print("Porte fermée. Mauvais QR code !")