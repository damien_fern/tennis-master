package handlers;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static org.slf4j.LoggerFactory.getLogger;

public class WebRequest {

    private static Logger logger = getLogger(WebRequest.class);

    public static String doRequest(String uri, String method, Map<String,  Object> params, String token) throws Exception {
        String result = "";

        if(method.equals("GET")) {
            URIBuilder builder = new URIBuilder(uri);
            params.forEach((key, value) -> builder.setParameter(key, String.valueOf(value)));
            logger.info("Params Get : {}", builder.getQueryParams().toString());
            HttpGet getRequest = new HttpGet(builder.build());
            if(token != null && !token.isEmpty()){
                getRequest.setHeader("Authorization", "Bearer " + token);
            }
            try (CloseableHttpClient httpClient = HttpClients.createDefault();
                 CloseableHttpResponse response = httpClient.execute(getRequest)) {
                if(response.getStatusLine().getStatusCode() != HttpStatus.SC_OK && response.getStatusLine().getStatusCode() != HttpStatus.SC_CREATED){
                    throw new Exception(String.valueOf(HttpStatus.SC_UNAUTHORIZED));
                }
                result = EntityUtils.toString(response.getEntity());
            }

        } else {
            HttpPost request = new HttpPost(uri);
            request.addHeader("content-type", "application/json");
            if(token != null && !token.isEmpty()){
                request.setHeader("Authorization", "Bearer " + token);
            }

            // Build JSON file
            AtomicInteger i = new AtomicInteger(1);
            StringBuilder json = new StringBuilder();
            json.append("{");
            params.forEach((key, value) -> {
                json.append("\"").append(key).append("\":");
                if(value instanceof String){
                    json.append("\"").append(value).append("\"");
                } else {
                    json.append(value);
                }
                if(i.get() < params.keySet().size()){
                    json.append(",");
                }
                i.getAndIncrement();
            });
            json.append("}");

            logger.info(json.toString());

            request.setEntity(new StringEntity(json.toString()));

            try (CloseableHttpClient httpClient = HttpClients.createDefault();
                 CloseableHttpResponse response = httpClient.execute(request)) {
                if(response.getStatusLine().getStatusCode() != HttpStatus.SC_OK && response.getStatusLine().getStatusCode() != HttpStatus.SC_CREATED){
                    throw new Exception(String.valueOf(HttpStatus.SC_UNAUTHORIZED));
                }
                result = EntityUtils.toString(response.getEntity());
            }
        }

        return result;
    }

}
