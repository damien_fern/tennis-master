package org.cpe.tennismaster.user.common.exception;

import org.cpe.tennismaster.common.exception.GeneralException;
import org.springframework.http.HttpStatus;

public class EmailAlreadyExistsException extends GeneralException {

    public EmailAlreadyExistsException(String adresse){
        super(adresse);
    }

    @Override
    protected HttpStatus defineStatusHttp() {
        return HttpStatus.CONFLICT;
    }

    @Override
    protected String defineErrorMessage(String variable) {
        return "L'adresse email " + variable + " existe déjà.";
    }

}
