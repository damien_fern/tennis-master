package org.cpe.tennismaster.user.common.vue;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.cpe.tennismaster.common.model.utilisateur.ClassementVue;
import org.cpe.tennismaster.common.model.utilisateur.Gender;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class UtilisateurAnnuaireVue {

    @NotNull
    private String nomUtilisateur;

    @NotNull
    private String prenomUtilisateur;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateNaissanceUtilisateur;

    @NotNull
    private Gender genreUtilisateur;

    private ClassementVue classement;

    public UtilisateurAnnuaireVue() {
    }

    public UtilisateurAnnuaireVue(String nomUtilisateur, String prenomUtilisateur, Date dateNaissanceUtilisateur) {
        this.nomUtilisateur = nomUtilisateur;
        this.prenomUtilisateur = prenomUtilisateur;
        this.dateNaissanceUtilisateur = dateNaissanceUtilisateur;
    }

    public String getNomUtilisateur() {
        return nomUtilisateur;
    }

    public void setNomUtilisateur(String nomUtilisateur) {
        this.nomUtilisateur = nomUtilisateur;
    }

    public String getPrenomUtilisateur() {
        return prenomUtilisateur;
    }

    public void setPrenomUtilisateur(String prenomUtilisateur) {
        this.prenomUtilisateur = prenomUtilisateur;
    }

    public Date getDateNaissanceUtilisateur() {
        return dateNaissanceUtilisateur;
    }

    public void setDateNaissanceUtilisateur(Date dateNaissanceUtilisateur) {
        this.dateNaissanceUtilisateur = dateNaissanceUtilisateur;
    }

    public Gender getGenreUtilisateur() {
        return genreUtilisateur;
    }

    public void setGenreUtilisateur(Gender genreUtilisateur) {
        this.genreUtilisateur = genreUtilisateur;
    }

    public ClassementVue getClassement() {
        return classement;
    }

    public void setClassement(ClassementVue classement) {
        this.classement = classement;
    }
}
