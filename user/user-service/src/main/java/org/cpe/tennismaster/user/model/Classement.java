package org.cpe.tennismaster.user.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "classement")
public class Classement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_classement", updatable = false)
    private Integer idClassement;

    @NotNull
    @Column(unique = true)
    private String libelleClassement;

    public Classement() {

    }

    public Classement(String libelleClassement) {
        this.libelleClassement = libelleClassement;
    }

    public Integer getIdClassement() {
        return idClassement;
    }

    public void setIdClassement(Integer idClassement) {
        this.idClassement = idClassement;
    }

    public String getLibelleClassement() {
        return libelleClassement;
    }

    public void setLibelleClassement(String libelleClassement) {
        this.libelleClassement = libelleClassement;
    }
}
