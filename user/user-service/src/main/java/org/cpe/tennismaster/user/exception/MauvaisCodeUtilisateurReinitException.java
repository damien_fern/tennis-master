package org.cpe.tennismaster.user.exception;

import org.cpe.tennismaster.common.exception.GeneralException;
import org.springframework.http.HttpStatus;

public class MauvaisCodeUtilisateurReinitException extends GeneralException {

    public MauvaisCodeUtilisateurReinitException() {
        super();
    }

    @Override
    protected HttpStatus defineStatusHttp() {
        return HttpStatus.BAD_REQUEST;
    }

    @Override
    protected String defineErrorMessage(String variable) {
        return "Le code de réinitialisation saisi n'est pas correct.";
    }

}
