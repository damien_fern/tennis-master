package org.cpe.tennismaster.user.exception;

import org.cpe.tennismaster.common.exception.GeneralException;
import org.springframework.http.HttpStatus;

public class MailNotCorrectException extends GeneralException {

    public MailNotCorrectException(String message) {
        super(message);
    }

    @Override
    protected HttpStatus defineStatusHttp() {
        return HttpStatus.BAD_REQUEST;
    }

    @Override
    protected String defineErrorMessage(String variable) {
        return variable;
    }
}
