package org.cpe.tennismaster.user.service;

import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.exception.ParameterException;
import org.cpe.tennismaster.user.common.dto.RoleDto;
import org.cpe.tennismaster.common.model.utilisateur.RoleVue;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RoleService {

    List<RoleVue> getAllRole();

    RoleVue getRoleById(Integer id) throws ParameterException;

    RoleVue createRole(RoleDto roleDto) throws NotFoundException;

    Void deleteRole(Integer id) throws NotFoundException;

    RoleVue updateRole(Integer id, RoleDto roleDto) throws NotFoundException;
}
