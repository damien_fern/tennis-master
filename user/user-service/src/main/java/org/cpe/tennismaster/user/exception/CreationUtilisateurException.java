package org.cpe.tennismaster.user.exception;

import org.cpe.tennismaster.common.exception.GeneralException;
import org.springframework.http.HttpStatus;

public class CreationUtilisateurException extends GeneralException {

    public CreationUtilisateurException() {
        super();
    }

    @Override
    protected HttpStatus defineStatusHttp() {
        return HttpStatus.BAD_REQUEST;
    }

    @Override
    protected String defineErrorMessage(String variable) {
        return "Impossible de créer l'utilisateur";
    }

}
