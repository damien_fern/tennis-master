package org.cpe.tennismaster.user.exception;

import org.cpe.tennismaster.common.exception.GeneralException;
import org.springframework.http.HttpStatus;

public class PasswordNotCorrectException extends GeneralException {

    public PasswordNotCorrectException() {
        super();
    }

    @Override
    protected HttpStatus defineStatusHttp() {
        return HttpStatus.BAD_REQUEST;
    }

    @Override
    protected String defineErrorMessage(String variable) {
        return "Le mot de passe ne respecte pas les contraintes.";
    }
}
