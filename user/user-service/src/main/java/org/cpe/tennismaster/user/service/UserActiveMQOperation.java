package org.cpe.tennismaster.user.service;

import org.cpe.tennismaster.common.configuration.activeMQ.GeneralActiveMQOperation;
import org.cpe.tennismaster.common.model.Letter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class UserActiveMQOperation extends GeneralActiveMQOperation {

    private final ModelMapper modelMapper;

    private final UserService userService;

    @Autowired
    public UserActiveMQOperation(ModelMapper modelMapper, @Lazy UserService userService) {
        this.modelMapper = modelMapper;
        this.userService = userService;
    }

    @Override
    protected void receiveMessage(Letter letter) {

    }

}
