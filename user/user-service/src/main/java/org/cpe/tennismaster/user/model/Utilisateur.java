package org.cpe.tennismaster.user.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;
import org.cpe.tennismaster.common.model.utilisateur.Gender;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "utilisateur")
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class Utilisateur {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, name = "id_utilisateur")
    private Integer idUtilisateur;

    @NotNull
    @Column(name = "nom_utilisateur", length = 20)
    private String nomUtilisateur;

    @NotNull
    @Column(name = "prenom_utilisateur", length = 20)
    private String prenomUtilisateur;

    @Column(unique = true, length = 100)
    @NotNull
    private String emailUtilisateur;

    @Column(unique = true, length = 20)
    @NotNull
    private String loginUtilisateur;

    @Column(name = "date_naissance_utilisateur")
    @Temporal(TemporalType.DATE)
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateNaissanceUtilisateur;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Column(name = "date_inscription_utilisateur")
    @Temporal(TemporalType.DATE)
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateInscriptionUtilisateur;

    @JsonProperty
    @Column(name = "date_derniere_connexion_utilisateur")
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateDerniereConnexionUtilisateur;

    @JsonProperty
    @Column(name = "date_activation_connexion_utilisateur")
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateActivationConnexionUtilisateur;

    @Convert(converter = GenderConverter.class)
    @NotNull
    private Gender genreUtilisateur;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotNull
    private String motPasseUtilisateur;

    @NotNull
    @Column(name = "adresse_1_utilisateur")
    private String adresse1Utilisateur;

    @Column(name = "adresse_2_utilisateur")
    private String adresse2Utilisateur;

    @NotNull
    @Column(length = 5)
    private String cpUtilisateur;

    @NotNull
    private String villeUtilisateur;

    private Boolean estActiveUtilisateur;

    private Boolean presenceAnnuaire;

    @Column(name = "token_phone_utilisateur")
    private String tokenPhone;

    //@JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ManyToOne
    @JoinColumn(name = "id_role", referencedColumnName = "id_role")
    private Role role;

    @ManyToOne
    @JoinColumn(name = "id_classement", referencedColumnName = "id_classement")
    private Classement classement;

    //TODO: à faire quand la table regle club sera créée
    /*@JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private int invitationCredit;*/

    @PrePersist
    void preInsert() {
        if (this.dateInscriptionUtilisateur == null) {
            this.dateInscriptionUtilisateur = new Date();
        }
        if(this.dateDerniereConnexionUtilisateur == null){
            this.dateDerniereConnexionUtilisateur = new Date();
        }
        if(this.estActiveUtilisateur == null){
            this.estActiveUtilisateur = false;
        }
        if(this.presenceAnnuaire == null) {
            this.presenceAnnuaire = true;
        }
    }

    public Utilisateur() {
    }

    public Utilisateur(String nomUtilisateur, String prenomUtilisateur, String emailUtilisateur, Gender genreUtilisateur) {
        this.nomUtilisateur = nomUtilisateur;
        this.prenomUtilisateur = prenomUtilisateur;
        this.emailUtilisateur = emailUtilisateur;
        this.genreUtilisateur = genreUtilisateur;
        this.dateInscriptionUtilisateur = new Date();
    }

    public Utilisateur(String nomUtilisateur, String prenomUtilisateur, String emailUtilisateur, Date dateInscriptionUtilisateur, Gender genreUtilisateur) {
        this.nomUtilisateur = nomUtilisateur;
        this.prenomUtilisateur = prenomUtilisateur;
        this.emailUtilisateur = emailUtilisateur;
        this.dateInscriptionUtilisateur = dateInscriptionUtilisateur;
        this.genreUtilisateur = genreUtilisateur;
    }

    public Integer getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(Integer idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public String getNomUtilisateur() {
        return nomUtilisateur;
    }

    public void setNomUtilisateur(String nomUtilisateur) {
        this.nomUtilisateur = nomUtilisateur;
    }

    public String getPrenomUtilisateur() {
        return prenomUtilisateur;
    }

    public void setPrenomUtilisateur(String prenomUtilisateur) {
        this.prenomUtilisateur = prenomUtilisateur;
    }

    public String getEmailUtilisateur() {
        return emailUtilisateur;
    }

    public void setEmailUtilisateur(String emailUtilisateur) {
        this.emailUtilisateur = emailUtilisateur;
    }

    public Date getDateInscriptionUtilisateur() {
        return dateInscriptionUtilisateur;
    }

    public void setDateInscriptionUtilisateur(Date dateInscriptionUtilisateur) {
        this.dateInscriptionUtilisateur = dateInscriptionUtilisateur;
    }

    public String getTokenPhone() {
        return tokenPhone;
    }

    public void setTokenPhone(String tokenPhone) {
        this.tokenPhone = tokenPhone;
    }

    public Gender getGenreUtilisateur() {
        return genreUtilisateur;
    }

    public void setGenreUtilisateur(Gender genreUtilisateur) {
        this.genreUtilisateur = genreUtilisateur;
    }

    public String getMotPasseUtilisateur() {
        return motPasseUtilisateur;
    }

    public void setMotPasseUtilisateur(String motPasseUtilisateur) {
        this.motPasseUtilisateur = motPasseUtilisateur;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Date getDateNaissanceUtilisateur() {
        return dateNaissanceUtilisateur;
    }

    public void setDateNaissanceUtilisateur(Date dateNaissanceUtilisateur) {
        this.dateNaissanceUtilisateur = dateNaissanceUtilisateur;
    }

    public Date getDateDerniereConnexionUtilisateur() {
        return dateDerniereConnexionUtilisateur;
    }

    public void setDateDerniereConnexionUtilisateur(Date dateDerniereConnexionUtilisateur) {
        this.dateDerniereConnexionUtilisateur = dateDerniereConnexionUtilisateur;
    }

    public Date getDateActivationConnexionUtilisateur() {
        return dateActivationConnexionUtilisateur;
    }

    public void setDateActivationConnexionUtilisateur(Date dateActivationConnexionUtilisateur) {
        this.dateActivationConnexionUtilisateur = dateActivationConnexionUtilisateur;
    }

    public Boolean getEstActiveUtilisateur() {
        return estActiveUtilisateur;
    }

    public void setEstActiveUtilisateur(Boolean estActiveUtilisateur) {
        this.estActiveUtilisateur = estActiveUtilisateur;
    }

    public String getLoginUtilisateur() {
        return loginUtilisateur;
    }

    public void setLoginUtilisateur(String loginUtilisateur) {
        this.loginUtilisateur = loginUtilisateur;
    }

    public String getAdresse1Utilisateur() {
        return adresse1Utilisateur;
    }

    public void setAdresse1Utilisateur(String adresse1Utilisateur) {
        this.adresse1Utilisateur = adresse1Utilisateur;
    }

    public String getAdresse2Utilisateur() {
        return adresse2Utilisateur;
    }

    public void setAdresse2Utilisateur(String adresse2Utilisateur) {
        this.adresse2Utilisateur = adresse2Utilisateur;
    }

    public String getCpUtilisateur() {
        return cpUtilisateur;
    }

    public void setCpUtilisateur(String cpUtilisateur) {
        this.cpUtilisateur = cpUtilisateur;
    }

    public String getVilleUtilisateur() {
        return villeUtilisateur;
    }

    public void setVilleUtilisateur(String villeUtilisateur) {
        this.villeUtilisateur = villeUtilisateur;
    }

    public Boolean getPresenceAnnuaire() {
        return presenceAnnuaire;
    }

    public void setPresenceAnnuaire(Boolean inPlayerDirectory) {
        presenceAnnuaire = inPlayerDirectory;
    }

    public Classement getClassement() {
        return classement;
    }

    public void setClassement(Classement classement) {
        this.classement = classement;
    }

    @Override
    public boolean equals(Object obj){
        if(!(obj instanceof Utilisateur)){
            return false;
        }

        Utilisateur utilisateur = (Utilisateur) obj;
        return this.idUtilisateur.equals(utilisateur.getIdUtilisateur())
                && this.nomUtilisateur.equals(utilisateur.getNomUtilisateur())
                && this.prenomUtilisateur.equals(utilisateur.getPrenomUtilisateur())
                && this.loginUtilisateur.equals(utilisateur.getLoginUtilisateur())
                && this.emailUtilisateur.equals(utilisateur.getEmailUtilisateur())
                && this.role.getLibelleRole().equals(utilisateur.getRole().getLibelleRole());
    }

    /*public Set<Long> getReservations() {
        return reservations;
    }

    public void setReservations(Set<Long> reservationsPlayer1) {
        this.reservations = reservationsPlayer1;
    }*/
}
