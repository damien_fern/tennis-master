package org.cpe.tennismaster.user.repository;

import org.cpe.tennismaster.user.model.ActivationRedis;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ActivationRedisRepository extends CrudRepository<ActivationRedis, Long> {
    ActivationRedis findByIdUser(Integer id);
}
