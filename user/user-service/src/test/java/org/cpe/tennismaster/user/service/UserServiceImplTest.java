package org.cpe.tennismaster.user.service;

import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.model.utilisateur.Gender;
import org.cpe.tennismaster.common.model.utilisateur.UtilisateurVue;
import org.cpe.tennismaster.common.utils.UtilsTools;
import org.cpe.tennismaster.user.model.Role;
import org.cpe.tennismaster.user.model.Utilisateur;
import org.cpe.tennismaster.user.repository.ActivationRedisRepository;
import org.cpe.tennismaster.user.repository.RoleRepository;
import org.cpe.tennismaster.user.repository.UserRepository;
import org.cpe.tennismaster.user.service.impl.UserServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import static org.mockito.Mockito.when;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    private ModelMapper modelMapper = new ModelMapper();

    @Mock
    private UtilsTools utilsTools;

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private ActivationRedisRepository activationRedisRepository;

    @Mock
    private UserActiveMQOperation userActiveMQOperation;

    private UserService userService;

    private List<Utilisateur> utilisateurs = new ArrayList<>();


    @Before
    public void setup(){
        this.userService = new UserServiceImpl(userRepository, roleRepository, activationRedisRepository, userActiveMQOperation, modelMapper, utilsTools);

        for(int i = 0; i < 10; i++){
            Utilisateur utilisateur = new Utilisateur();
            utilisateur.setIdUtilisateur(i+1);
            utilisateur.setNomUtilisateur("Test");
            utilisateur.setPrenomUtilisateur("Unitaire");
            utilisateur.setEmailUtilisateur("test" + i + "@test.fr");
            utilisateur.setLoginUtilisateur("test" + i);
            utilisateur.setDateNaissanceUtilisateur(new Date());
            utilisateur.setGenreUtilisateur(Gender.H);
            utilisateur.setMotPasseUtilisateur("test");
            utilisateur.setAdresse1Utilisateur("");
            utilisateur.setCpUtilisateur("69006");
            utilisateur.setVilleUtilisateur("Lyon");
            utilisateur.setRole(new Role("USER", "ROLE_USER"));
            utilisateurs.add(utilisateur);
        }

        when(userRepository.findAllBy()).thenReturn(utilisateurs);

        Utilisateur utilisateur = utilisateurs.get(0);
        when(userRepository.findById(utilisateur.getIdUtilisateur())).thenReturn(Optional.of(utilisateur));
        when(userRepository.findByEmailUtilisateur(utilisateur.getEmailUtilisateur())).thenReturn(Optional.of(utilisateur));
    }

    @Test
    public void whenGetAllUtilisateur_thenAllUtilisateurVueShouldBeFound() {
        Type listType = new TypeToken<List<UtilisateurVue>>() {}.getType();
        List<UtilisateurVue> utilisateurVues = modelMapper.map(utilisateurs, listType);

        List<UtilisateurVue> utilisateurs = userService.getAllUsers();

        Assert.assertEquals(utilisateurs, utilisateurVues);
    }

    @Test
    public void whenGetUtilisateurById_thenUtilisateurShouldBeFound() {
        Utilisateur utilisateur = utilisateurs.get(0);
        UtilisateurVue utilisateurVue = modelMapper.map(utilisateur, UtilisateurVue.class);

        //First id value in the list
        UtilisateurVue result = userService.getUserById(1);

        Assert.assertEquals(utilisateurVue, result);
    }

    @Test(expected = NotFoundException.class)
    public void whenGetUtilisateurByIdNotExist_thenNotFoundExceptionShouldBeThrown() {
        //Not in the list
        userService.getUserById(62);
    }

    @Test
    public void whenGetUtilisateurByEmail_thenUtilisateurShouldBeFound() {
        Utilisateur utilisateur = utilisateurs.get(0);
        UtilisateurVue utilisateurVue = modelMapper.map(utilisateur, UtilisateurVue.class);

        //First id value in the list
        UtilisateurVue result = userService.getUserByEmail(utilisateur.getEmailUtilisateur());

        Assert.assertEquals(utilisateurVue, result);
    }

    @Test(expected = NotFoundException.class)
    public void whenGetUtilisateurByEmailNotExist_thenNotFoundExceptionShouldBeThrown() {
        //Not in the list
        userService.getUserByEmail("notexist");
    }

    /*@Test
    public void whenDeleteUtilisateur_thenNullShouldBeReturned() {
        Utilisateur utilisateur = utilisateurs.get(0);

        //First id value in the list
        Void result = userService.deleteUser(utilisateur.getIdUtilisateur());

        Assert.assertNull(result);
    }*/

    @Test(expected = NotFoundException.class)
    public void whenDeleteUtilisateurNotExist_thenNotFoundExceptionShouldBeThrown() {
        //Not in the list
        userService.deleteUser(60);
    }



}
