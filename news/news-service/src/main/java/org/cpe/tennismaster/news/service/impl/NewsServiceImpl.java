package org.cpe.tennismaster.news.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.exception.ParameterException;
import org.cpe.tennismaster.common.utils.UtilsTools;
import org.cpe.tennismaster.news.common.dto.NewsDto;
import org.cpe.tennismaster.news.common.vue.NewsVue;
import org.cpe.tennismaster.news.model.News;
import org.cpe.tennismaster.news.repository.NewsRepository;
import org.cpe.tennismaster.news.service.NewsService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class NewsServiceImpl implements NewsService {

    private final ModelMapper modelMapper;

    private final NewsRepository newsRepository;

    @Autowired
    public NewsServiceImpl(NewsRepository newsRepository, ModelMapper modelMapper) {
        this.newsRepository = newsRepository;
        this.modelMapper = modelMapper;
    }

    public List<NewsVue> getAllNews() {
        Optional<List<News>> optNews =  this.newsRepository.findAllBy();
        if(optNews.isPresent()) {
            Type listType = new TypeToken<List<NewsVue>>() {}.getType();
            return modelMapper.map(optNews.get(), listType);
        }
        return Collections.<NewsVue>emptyList();
    }

    public NewsVue getNewsById(Integer id) throws ParameterException {
        Optional<News> optNews = newsRepository.findById(id);
        if (!optNews.isPresent()) {
            throw new ParameterException();
        }
        return modelMapper.map(optNews.get(), NewsVue.class);
    }

    public NewsVue createNews(NewsDto newsDto) throws NotFoundException {
        if (newsDto == null) {
            throw new NotFoundException("News");
        }
        News newsModel = modelMapper.map(newsDto, News.class);
        newsModel = newsRepository.save(newsModel);
        return modelMapper.map(newsModel, NewsVue.class);
    }

    public Void deleteNews(Integer id) throws NotFoundException {
        Optional<News> optNews = newsRepository.findById(id);
        if (!optNews.isPresent()) {
            throw new NotFoundException("News");
        }
        newsRepository.delete(optNews.get());
        return null;
    }

    public NewsVue updateNews(Integer id, NewsDto newsDto) throws NotFoundException {
        if (newsDto == null) {
            throw new NotFoundException("News");
        }
        Optional<News> optExistingNews = newsRepository.findById(id);
        if (!optExistingNews.isPresent()) {
            throw new NotFoundException("News");
        }
        News newsModel = modelMapper.map(newsDto, News.class);
        UtilsTools.copyNonNullProperties(newsModel, optExistingNews.get());
        News newsModelUpdated = newsRepository.save(optExistingNews.get());

        return modelMapper.map(newsModelUpdated, NewsVue.class);
    }

}
