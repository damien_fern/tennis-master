package org.cpe.tennismaster.news.controller;

import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.exception.ParameterException;
import org.cpe.tennismaster.common.roles.Roles;
import org.cpe.tennismaster.news.common.dto.NewsDto;
import org.cpe.tennismaster.news.common.vue.NewsVue;
import org.cpe.tennismaster.news.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/news")
public class NewsController {

    private final NewsService newsService;

    @Autowired
    public NewsController(NewsService newsService) {
        this.newsService = newsService;
    }

    @GetMapping
    public ResponseEntity<List<NewsVue>> getAllNews() {
        return ResponseEntity.ok(newsService.getAllNews());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<NewsVue> getNewsById(@PathVariable Integer id) throws ParameterException {
        return ResponseEntity.ok(newsService.getNewsById(id));
    }

    @PostMapping
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity<NewsVue> createNews(@RequestBody NewsDto newsDto) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.CREATED).body(newsService.createNews(newsDto));
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity<Void> deleteNews(@PathVariable Integer id) throws NotFoundException {
        return ResponseEntity.ok(newsService.deleteNews(id));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity<NewsVue> updateNews(@PathVariable Integer id, @RequestBody NewsDto newsDto) throws NotFoundException {
        return ResponseEntity.ok(newsService.updateNews(id, newsDto));
    }

}
