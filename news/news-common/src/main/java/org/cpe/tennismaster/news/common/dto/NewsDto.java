package org.cpe.tennismaster.news.common.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NewsDto {

    @NotNull
    private String titreNews;

    @NotNull
    private String contenuNews;

    private String imageUrl;

    public NewsDto() {
    }

    public String getTitreNews() {
        return titreNews;
    }

    public void setTitreNews(String titreNews) {
        this.titreNews = titreNews;
    }

    public String getContenuNews() {
        return contenuNews;
    }

    public void setContenuNews(String contenuNews) {
        this.contenuNews = contenuNews;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
