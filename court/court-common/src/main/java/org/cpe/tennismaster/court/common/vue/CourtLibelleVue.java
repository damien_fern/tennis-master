package org.cpe.tennismaster.court.common.vue;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CourtLibelleVue {

    @NotNull
    private String libelleCourt;

    public CourtLibelleVue() {
    }

    public CourtLibelleVue(String name) {
        this.libelleCourt = name;
    }

    public String getLibelleCourt() {
        return this.libelleCourt;
    }

    public void setLibelleCourt(String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }
}
