package org.cpe.tennismaster.court.common.dto;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class CourtDto {

    @NotNull
    private String libelleCourt;

    private String localisationCourt;

    private BigDecimal latitudeCourt;

    private BigDecimal longitudeCourt;

    private Boolean couvert;

    private Boolean reservable;

    private TypeCourtDto typeCourt;

    public CourtDto() {
    }

    public CourtDto(String name) {
        this.libelleCourt = name;
    }

    public String getLibelleCourt() {
        return this.libelleCourt;
    }

    public void setLibelleCourt(String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }

    public String getLocalisationCourt() {
        return this.localisationCourt;
    }

    public void setLocalisationCourt(String localisationCourt) {
        this.localisationCourt = localisationCourt;
    }

    public BigDecimal getLatitudeCourt() {
        return this.latitudeCourt;
    }

    public void setLatitudeCourt(BigDecimal latitudeCourt) {
        this.latitudeCourt = latitudeCourt;
    }

    public BigDecimal getLongitudeCourt() {
        return this.longitudeCourt;
    }

    public void setLongitudeCourt(BigDecimal longitudeCourt) {
        this.longitudeCourt = longitudeCourt;
    }

    public Boolean getCouvert() {
        return this.couvert;
    }

    public void setCouvert(Boolean couvert) {
        this.couvert = couvert;
    }

    public Boolean getReservable() {
        return this.reservable;
    }

    public void setReservable(Boolean reservable) {
        this.reservable = reservable;
    }

    public TypeCourtDto getTypeCourt() {
        return typeCourt;
    }

    public void setTypeCourt(TypeCourtDto typeCourt) {
        this.typeCourt = typeCourt;
    }
}
