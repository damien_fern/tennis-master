package org.cpe.tennismaster.court.common.vue;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.NotNull;
import java.util.Set;

public class TypeCourtVue {

    @NotNull
    private Integer idTypeCourt;

    @NotNull
    private String libelleTypeCourt;

    @JsonIgnore
    private Set<CourtVue> courts;

    public TypeCourtVue() {
    }

    public TypeCourtVue(String libelleTypeCourt) {
        this.libelleTypeCourt = libelleTypeCourt;
    }

    public Integer getIdTypeCourt() {
        return idTypeCourt;
    }

    public void setIdTypeCourt(Integer idTypeCourt) {
        this.idTypeCourt = idTypeCourt;
    }

    public String getLibelleTypeCourt() {
        return libelleTypeCourt;
    }

    public void setLibelleTypeCourt(String labelType) {
        this.libelleTypeCourt = labelType;
    }

    @JsonIgnore
    public Set<CourtVue> getCourts() {
        return courts;
    }

    public void setCourts(Set<CourtVue> courtVues) {
        this.courts = courtVues;
    }
}