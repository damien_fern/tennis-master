package org.cpe.tennismaster.court.common.vue;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CourtVue {

    @NotNull
    private Integer idCourt;

    @NotNull
    private String libelleCourt;

    private String localisationCourt;

    private BigDecimal latitudeCourt;

    private BigDecimal longitudeCourt;

    private Boolean couvert;

    private Boolean reservable;

    private TypeCourtVue typeCourt;

    public CourtVue() {
    }

    public CourtVue(String name) {
        this.libelleCourt = name;
    }

    public Integer getIdCourt() {
        return this.idCourt;
    }

    public void setIdCourt(Integer idCourt) {
        this.idCourt = idCourt;
    }

    public String getLibelleCourt() {
        return this.libelleCourt;
    }

    public void setLibelleCourt(String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }

    public String getLocalisationCourt() {
        return this.localisationCourt;
    }

    public void setLocalisationCourt(String localisationCourt) {
        this.localisationCourt = localisationCourt;
    }

    public BigDecimal getLatitudeCourt() {
        return this.latitudeCourt;
    }

    public void setLatitudeCourt(BigDecimal latitudeCourt) {
        this.latitudeCourt = latitudeCourt;
    }

    public BigDecimal getLongitudeCourt() {
        return this.longitudeCourt;
    }

    public void setLongitudeCourt(BigDecimal longitudeCourt) {
        this.longitudeCourt = longitudeCourt;
    }

    public Boolean getCouvert() {
        return this.couvert;
    }

    public void setCouvert(Boolean couvert) {
        this.couvert = couvert;
    }

    public Boolean getReservable() {
        return this.reservable;
    }

    public void setReservable(Boolean reservable) {
        this.reservable = reservable;
    }

    public TypeCourtVue getTypeCourt() {
        return typeCourt;
    }

    public void setTypeCourt(TypeCourtVue typeCourt) {
        this.typeCourt = typeCourt;
    }

    /*@Override
    public String toString() {
        String str = "Court{" +
                "\n\tid=" + id +
                "\n\tname='" + name + '\'' +
                "\n\treservations=";
        if(this.getReservations().isEmpty()) {
            str += "empty";
        } else {
            for (Reservation reservation : this.getReservations()) {
                str += reservation.getUserOne().getFirstName() + " - ";
                if (reservation.getUserTwo() != null) {
                    str += reservation.getUserTwo().getFirstName();
                } else {
                    str += "null";
                }
            }
        }
        str += '}';
        return str;
    }*/
}
