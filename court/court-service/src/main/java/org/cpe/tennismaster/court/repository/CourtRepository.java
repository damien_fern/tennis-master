package org.cpe.tennismaster.court.repository;

import org.cpe.tennismaster.court.model.Court;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CourtRepository extends JpaRepository<Court, Integer> {

    Optional<List<Court>> findAllBy();

    Optional<List<Court>> findByIdCourt(Integer idCourt);

    Optional<List<Court>> findByLibelleCourt(String libelleCourt);

    @Query("select c.idCourt from Court c")
    List<Integer> findAllIds();

    @Query("select c from Court c where c.couvert = :isCovered")
    Optional<List<Court>> findAllIdsByIsCovered(@Param("isCovered") Boolean isCovered);

    @Query("select c from Court c where c.typeCourt.idTypeCourt = :idCourtKind")
    Optional<List<Court>> findAllIdsByCourtKindId(@Param("idCourtKind") Integer idCourtKind);

    @Query("select c from Court c where c.couvert = :isCovered and c.typeCourt.idTypeCourt = :idCourtKind")
    Optional<List<Court>> findAllIdsByIsCoveredAndCourtKindId(@Param("isCovered") Boolean isCovered, @Param("idCourtKind") Integer idCourtKind);



}
