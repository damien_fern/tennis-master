package org.cpe.tennismaster.court.controller;

import org.cpe.tennismaster.common.roles.Roles;
import org.cpe.tennismaster.court.common.dto.TypeCourtDto;
import org.cpe.tennismaster.court.common.vue.TypeCourtVue;
import org.cpe.tennismaster.court.service.CourtKindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/courtKind")
public class CourtKindController {

    @Autowired
    private CourtKindService courtKindService;

    @GetMapping
    public ResponseEntity<List<TypeCourtVue>> getAllCourtsKind() {
        return ResponseEntity.ok(courtKindService.getAllCourtsKind());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<TypeCourtVue> getCourtKindById(@PathVariable Integer id) {
        return ResponseEntity.ok(courtKindService.getCourtKindById(id));
    }

    @GetMapping(value = "label/{courtKind}")
    public ResponseEntity<TypeCourtVue> getCourtKindByLabel(@PathVariable String courtKind) {
        return ResponseEntity.ok(courtKindService.getCourtKindByLabel(courtKind));
    }

    @PostMapping
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity<TypeCourtVue> createCourtKind(@RequestBody TypeCourtDto typeCourtDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(courtKindService.createCourtKind(typeCourtDto));
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity deleteCourtKind(@PathVariable Integer id) {
        return ResponseEntity.ok(courtKindService.deleteCourtKind(id));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('"+ Roles.Libelle.ADMIN +"')")
    public ResponseEntity<TypeCourtVue> updateCourtKind(@PathVariable Integer id, @RequestBody TypeCourtDto typeCourtDto) {
        return ResponseEntity.ok(courtKindService.updateCourtKind(id, typeCourtDto));
    }
}
