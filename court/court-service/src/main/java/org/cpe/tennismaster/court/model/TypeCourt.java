package org.cpe.tennismaster.court.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "type_court")
public class TypeCourt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_type_court", updatable = false)
    private Integer idTypeCourt;

    @Column(unique = true, length = 50)
    @NotNull
    private String libelleTypeCourt;

    @JsonIgnore
    @OneToMany(mappedBy = "typeCourt")
    private Set<Court> courts;

    public TypeCourt() {
    }

    public TypeCourt(String libelleTypeCourt) {
        this.libelleTypeCourt = libelleTypeCourt;
    }

    public Integer getIdTypeCourt() {
        return idTypeCourt;
    }

    public void setIdTypeCourt(Integer idTypeCourt) {
        this.idTypeCourt = idTypeCourt;
    }

    public String getLibelleTypeCourt() {
        return libelleTypeCourt;
    }

    public void setLibelleTypeCourt(String labelType) {
        this.libelleTypeCourt = labelType;
    }

    @JsonIgnore
    public Set<Court> getCourts() {
        return courts;
    }

    public void setCourts(Set<Court> courts) {
        this.courts = courts;
    }
}