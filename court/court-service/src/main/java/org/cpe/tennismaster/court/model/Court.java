package org.cpe.tennismaster.court.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Table(name = "court")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Court {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, name = "id_court")
    private Integer idCourt;

    @Column(unique = true)
    @NotNull
    private String libelleCourt;

    private String localisationCourt;

    private BigDecimal latitudeCourt;

    private BigDecimal longitudeCourt;

    private Boolean couvert;

    private Boolean reservable;

    @ManyToOne
    @JoinColumn(name = "id_type_court", referencedColumnName = "id_type_court")
    private TypeCourt typeCourt;

    //private Set<Long> reservations;

    @PrePersist
    void preInsert() {
        if(this.couvert == null) { //TODO: à laisser ?
            this.couvert = false;
        }
        if(this.reservable == null) {
            this.reservable = true;
        }
    }

    public Court() {
    }

    public Court(String name) {
        this.libelleCourt = name;
    }

    public Integer getIdCourt() {
        return this.idCourt;
    }

    public void setIdCourt(Integer idCourt) {
        this.idCourt = idCourt;
    }

    public String getLibelleCourt() {
        return this.libelleCourt;
    }

    public void setLibelleCourt(String libelleCourt) {
        this.libelleCourt = libelleCourt;
    }

    public String getLocalisationCourt() {
        return this.localisationCourt;
    }

    public void setLocalisationCourt(String localisationCourt) {
        this.localisationCourt = localisationCourt;
    }

    public BigDecimal getLatitudeCourt() {
        return this.latitudeCourt;
    }

    public void setLatitudeCourt(BigDecimal latitudeCourt) {
        this.latitudeCourt = latitudeCourt;
    }

    public BigDecimal getLongitudeCourt() {
        return this.longitudeCourt;
    }

    public void setLongitudeCourt(BigDecimal longitudeCourt) {
        this.longitudeCourt = longitudeCourt;
    }

    public Boolean getCouvert() {
        return this.couvert;
    }

    public void setCouvert(Boolean couvert) {
        this.couvert = couvert;
    }

    public Boolean getReservable() {
        return this.reservable;
    }

    public void setReservable(Boolean reservable) {
        this.reservable = reservable;
    }

    public TypeCourt getTypeCourt() {
        return typeCourt;
    }

    public void setTypeCourt(TypeCourt typeCourt) {
        this.typeCourt = typeCourt;
    }

    /*@Override
    public String toString() {
        String str = "Court{" +
                "\n\tid=" + id +
                "\n\tname='" + name + '\'' +
                "\n\treservations=";
        if(this.getReservations().isEmpty()) {
            str += "empty";
        } else {
            for (Reservation reservation : this.getReservations()) {
                str += reservation.getUserOne().getFirstName() + " - ";
                if (reservation.getUserTwo() != null) {
                    str += reservation.getUserTwo().getFirstName();
                } else {
                    str += "null";
                }
            }
        }
        str += '}';
        return str;
    }*/
}
