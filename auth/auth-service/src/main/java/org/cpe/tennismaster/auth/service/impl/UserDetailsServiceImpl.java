package org.cpe.tennismaster.auth.service.impl;

import org.cpe.tennismaster.auth.model.CustomUserDetails;
import org.cpe.tennismaster.auth.model.UtilisateurVue;
import org.cpe.tennismaster.auth.service.AuthorizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    @Autowired
    private AuthorizationService authorizationService;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        UtilisateurVue utilisateurVue = authorizationService.getUser(login);

        if(utilisateurVue == null) {
            throw new UsernameNotFoundException("Utilisateur non trouvé.");
        }

        return new CustomUserDetails(utilisateurVue);
    }

}
