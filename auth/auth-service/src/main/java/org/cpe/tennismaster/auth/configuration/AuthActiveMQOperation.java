package org.cpe.tennismaster.auth.configuration;

import org.cpe.tennismaster.common.configuration.activeMQ.GeneralActiveMQOperation;
import org.cpe.tennismaster.common.model.Intent;
import org.cpe.tennismaster.common.model.Letter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class AuthActiveMQOperation extends GeneralActiveMQOperation {

    @Autowired
    public AuthActiveMQOperation(/*@Lazy UpdateDateRedisService updateDateRedisService*/) {
    }

    @Override
    protected void receiveMessage(Letter letter) {
        Intent intent = letter.getIntentAction();
        switch(intent){

        }
    }

}
