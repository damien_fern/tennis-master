package org.cpe.tennismaster.auth.configuration;

import org.cpe.tennismaster.auth.model.CustomUserDetails;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.event.EventListener;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.endpoint.AuthorizationEndpoint;
import org.springframework.security.oauth2.provider.token.*;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;

import javax.annotation.PostConstruct;
import java.security.KeyPair;
import java.util.Arrays;
import java.util.Map;

@Configuration
@EnableAuthorizationServer
@EnableConfigurationProperties(SecurityProperties.class)
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

    public static final String clientIdAdminWeb = "adminweb";
    public static final String clientIdCourtService = "courtservice";
    public static final String clientIdUserService = "userservice";
    public static final String clientIdReservationService = "reservationservice";
    public static final String clientIdNewsService = "newsservice";
    public static final String clientIdAmazon = "amazon";

    private final AuthActiveMQOperation authActiveMQOperation;

    private final SecurityProperties securityProperties;

    private final RedisConnectionFactory redisConnectionFactory;

    private final AuthenticationManager authenticationManager;

    private final AuthorizationEndpoint authorizationEndpoint;

    private final PasswordEncoder passwordEncoder;

    private JwtAccessTokenConverter jwtAccessTokenConverter;
    private TokenStore tokenStore;

    public AuthorizationServerConfiguration(AuthenticationManager authenticationManager, RedisConnectionFactory redisConnectionFactory, SecurityProperties securityProperties, PasswordEncoder passwordEncoder, AuthActiveMQOperation authActiveMQOperation, @Lazy AuthorizationEndpoint authorizationEndpoint) {
        this.authenticationManager = authenticationManager;
        this.redisConnectionFactory = redisConnectionFactory;
        this.securityProperties = securityProperties;
        this.passwordEncoder = passwordEncoder;
        this.authActiveMQOperation = authActiveMQOperation;
        this.authorizationEndpoint = authorizationEndpoint;
    }

    @EventListener
    public void authSuccessEventListener(AuthenticationSuccessEvent authorizedEvent){
        Authentication authentication = authorizedEvent.getAuthentication();
        if(authentication.getPrincipal() instanceof CustomUserDetails && authentication.isAuthenticated()){
            // Authentication success
        }
    }

//    @PostConstruct
//    public void init() {
//        authorizationEndpoint.setUserApprovalPage("forward:/oauth/confirm_access");
//    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        if (jwtAccessTokenConverter != null) {
            return jwtAccessTokenConverter;
        }

        SecurityProperties.JwtProperties jwtProperties = securityProperties.getJwt();
        KeyPair keyPair = keyPair(jwtProperties, keyStoreKeyFactory(jwtProperties));

        jwtAccessTokenConverter = new JwtAccessTokenConverter();
        jwtAccessTokenConverter.setKeyPair(keyPair);
        jwtAccessTokenConverter.setAccessTokenConverter(authExtractor());
        return jwtAccessTokenConverter;
    }

    @Bean
    public TokenEnhancer customTokenEnhancer() {
        return new CustomTokenEnhancer();
    }

    @Override
    public void configure (AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        TokenEnhancerChain enhancerChain = new TokenEnhancerChain();
        enhancerChain.setTokenEnhancers(Arrays.asList(customTokenEnhancer(), accessTokenConverter()));

        endpoints
                .authenticationManager(this.authenticationManager)
                .tokenStore(redisTokenStore())
                .tokenEnhancer(enhancerChain);
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security
                .passwordEncoder(this.passwordEncoder)
                .tokenKeyAccess("permitAll()")
                .checkTokenAccess("isAuthenticated()");
    }

    @Bean
    public DefaultTokenServices tokenServices(@Qualifier("redisTokenStore") final TokenStore tokenStore,
                                              final ClientDetailsService clientDetailsService) {
        DefaultTokenServices tokenServices = new DefaultTokenServices();
        tokenServices.setSupportRefreshToken(true);
        tokenServices.setTokenStore(tokenStore);
        tokenServices.setClientDetailsService(clientDetailsService);
        tokenServices.setAuthenticationManager(this.authenticationManager);
        return tokenServices;
    }

    @Bean
    public TokenStore redisTokenStore() {
        if(tokenStore == null){
            tokenStore = new RedisTokenStore(redisConnectionFactory);
        }
        return tokenStore;
    }

    @Bean
    public DefaultAccessTokenConverter authExtractor() {
        return new DefaultAccessTokenConverter() {
            @Override
            public OAuth2Authentication extractAuthentication(Map<String, ?> claims) {
                OAuth2Authentication authentication = super.extractAuthentication(claims);
                authentication.setDetails(claims);
                return authentication;
            }
        };
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient(clientIdAdminWeb)
                .secret("{noop}adminweb-pwd")
                .authorizedGrantTypes("password", "refresh_token", "client_credentials")
                .scopes("read", "write")
                .accessTokenValiditySeconds(20000)
                .refreshTokenValiditySeconds(40000)

                .and()

                .withClient(clientIdCourtService)
                .secret("{noop}usercourt-pWd")
                .authorizedGrantTypes("password", "refresh_token", "client_credentials")
                .scopes("read", "write")
                .accessTokenValiditySeconds(20000)
                .refreshTokenValiditySeconds(40000)

                .and()

                .withClient(clientIdUserService)
                .secret("{noop}userservice-pWd")
                .authorizedGrantTypes("password", "refresh_token", "client_credentials")
                .scopes("read", "write")
                .accessTokenValiditySeconds(20000)
                .refreshTokenValiditySeconds(40000)

                .and()

                .withClient(clientIdReservationService)
                .secret("{noop}reservationservice-pWd")
                .authorizedGrantTypes("password", "refresh_token", "client_credentials")
                .scopes("read", "write")
                .accessTokenValiditySeconds(20000)
                .refreshTokenValiditySeconds(40000)

                .and()

                .withClient(clientIdNewsService)
                .secret("{noop}newsservice-pWd")
                .authorizedGrantTypes("password", "refresh_token", "client_credentials")
                .scopes("read", "write")
                .accessTokenValiditySeconds(20000)
                .refreshTokenValiditySeconds(40000)

                .and()

                .withClient(clientIdAmazon)
                .secret("{noop}irccpe69")
                .authorizedGrantTypes("client_credentials", "password", "authorization_code", "refresh_token")
                .scopes("read", "write")
                .redirectUris("https://getpostman.com/oauth2/callback", "https://layla.amazon.com/api/skill/link/M2IDGPX1RYFLND", "https://pitangui.amazon.com/api/skill/link/M2IDGPX1RYFLND", "https://alexa.amazon.co.jp/api/skill/link/M2IDGPX1RYFLND")

        ;
    }

    private KeyPair keyPair(SecurityProperties.JwtProperties jwtProperties, KeyStoreKeyFactory keyStoreKeyFactory) {
        return keyStoreKeyFactory.getKeyPair(jwtProperties.getKeyPairAlias(), jwtProperties.getKeyPairPassword().toCharArray());
    }

    private KeyStoreKeyFactory keyStoreKeyFactory(SecurityProperties.JwtProperties jwtProperties) {
        return new KeyStoreKeyFactory(jwtProperties.getKeyStore(), jwtProperties.getKeyStorePassword().toCharArray());
    }

}
