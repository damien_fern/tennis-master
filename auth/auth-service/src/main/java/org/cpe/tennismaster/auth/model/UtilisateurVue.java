package org.cpe.tennismaster.auth.model;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;
import org.hibernate.annotations.Synchronize;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Immutable
@Subselect(
        "SELECT id_utilisateur as id, login_utilisateur as login, mot_passe_utilisateur as mot_passe, est_active_utilisateur as est_active, r.libelle_role_auth as role " +
                "FROM public.utilisateur u " +
                "JOIN public.role r ON r.id_role = u.id_role"
)
@Synchronize({"public.utilisateur"})
public class UtilisateurVue {

    @Id
    private Integer id;

    private String login;

    private String mot_passe;

    private Boolean est_active;

    private String role;

    public Integer getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getMot_passe() {
        return mot_passe;
    }

    public Boolean getEst_active() {
        return est_active;
    }

    public String getRole() {
        return role;
    }

}
