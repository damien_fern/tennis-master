package org.cpe.tennismaster.auth.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CustomUserDetails implements UserDetails {

    private Collection<? extends GrantedAuthority> authorities;
    private String password;
    private String username;
    private Integer id;
    private Boolean isActivated;

    public CustomUserDetails(UtilisateurVue utilisateurVue) {
        this.id = utilisateurVue.getId();
        this.username = utilisateurVue.getLogin();
        this.password = utilisateurVue.getMot_passe();
        this.isActivated = utilisateurVue.getEst_active();
        this.authorities = translate(utilisateurVue.getRole());
    }

    /**
     * Translates the role to a List<GrantedAuthority>
     * @param role the input list of roles.
     * @return a list of granted authorities
     */
    private Collection<? extends GrantedAuthority> translate(String role) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        String name = role.toUpperCase();
        //Make sure that all roles start with "ROLE_"
        if (!name.startsWith("ROLE_"))
            name = "ROLE_" + name;
        authorities.add(new SimpleGrantedAuthority(name));
        return authorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return isActivated;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
