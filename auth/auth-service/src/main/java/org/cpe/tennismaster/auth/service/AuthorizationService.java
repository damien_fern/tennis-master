package org.cpe.tennismaster.auth.service;

import org.cpe.tennismaster.auth.model.UtilisateurVue;
import org.springframework.stereotype.Service;

@Service
public interface AuthorizationService {

    UtilisateurVue getUser(String email);

    String generatePassword(String password);

}
