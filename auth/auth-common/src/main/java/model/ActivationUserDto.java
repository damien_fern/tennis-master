package model;

import lombok.*;

import javax.validation.constraints.NotEmpty;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ActivationUserDto {

    @NotEmpty
    private String email;

    @NotEmpty
    private String activationId;

}
