'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  AUTH_SERVER: '"http://localhost:8081"',
  COURT_SERVER: '"http://localhost:8082"',
  USER_SERVER: '"http://localhost:8083"',
  RESERVATION_SERVER: '"http://localhost:8084"',
  REGLES_SERVER: '"http://localhost:8085"',
  NEWS_SERVER: '"http://localhost:8087"'
})
