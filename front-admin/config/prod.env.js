'use strict'
module.exports = {
  NODE_ENV: '"production"',
  AUTH_SERVER: '"https://valentinguevara.site/auth-service"',
  COURT_SERVER: '"https://valentinguevara.site/court-service"',
  USER_SERVER: '"https://valentinguevara.site/user-service"',
  RESERVATION_SERVER: '"https://valentinguevara.site/reservation-service"',
  REGLES_SERVER: '"https://valentinguevara.site/reglesClub-service"',
  NEWS_SERVER: '"https://valentinguevara.site/news-service"'
}
