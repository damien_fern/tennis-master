import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'
import localForage from 'localforage'
import moduleUser from './modules/users'
import moduleReservation from './modules/reservations'
import moduleCourt from './modules/courts'
import moduleNews from './modules/news'
import moduleAuth from './modules/auth'
Vue.use(Vuex)

const vuexStorage = new VuexPersist({
  key: 'tennismaster',
  storage: localForage,
  asyncStorage: true
})

export default new Vuex.Store({
  plugins: [vuexStorage.plugin],
  modules: {
    moduleUser,
    moduleReservation,
    moduleCourt,
    moduleNews,
    moduleAuth
  }
})
