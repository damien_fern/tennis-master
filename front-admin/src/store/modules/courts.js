import courts from '../../api/courts'

const moduleCourt = {
  state: {
    all: []
  },
  mutations: {
    setCourts (state, courts) {
      state.all = courts
    }
  },
  actions: {
    getAllCourts ({commit}) {
      courts.getCourts(courts => {
        commit('setCourts', courts)
      })
    }
  },
  getters: {
  }
}

export default moduleCourt
