import news from '../../api/news'

const moduleNews = {
  state: {
    all: []
  },
  mutations: {
    setNews (state, news) {
      state.all = news
    }
  },
  actions: {
    getAllNews ({commit}) {
      news.getNews(news => {
        commit('setNews', news)
      })
    }
  },
  getters: {
  }
}

export default moduleNews
