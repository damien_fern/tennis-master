import users from '../../api/users'

const moduleUser = {
  state: {
    all: []
  },
  mutations: {
    setUsers (state, users) {
      state.all = users
    }
  },
  actions: {
    getAllUsers ({commit}) {
      users.getUsers(users => {
        commit('setUsers', users)
      })
    }
  },
  getters: {
  }
}

export default moduleUser
