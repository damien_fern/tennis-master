import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import PasswordReset from '@/components/PasswordReset'
import Adherents from '@/components/adherents/Adherents'
import AdherentForm from '@/components/adherents/AdherentForm'
import AdherentFiche from '@/components/adherents/AdherentFiche'
import News from '@/components/news/News'
import NewsForm from '@/components/news/NewsForm'
import Config from '@/components/Config'
import Courts from '@/components/courts/Courts'
import CourtForm from '@/components/courts/CourtForm'
import CourtFiche from '@/components/courts/CourtFiche'
import Reservations from '@/components/reservations/Reservations'
import ReservationForm from '@/components/reservations/ReservationForm'
import ReservationFiche from '@/components/reservations/ReservationFiche'
import BadGateway from '@/components/errorPages/BadGateway'
import store from '@/store'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      alias: '/',
      path: '/adherents',
      name: 'Adherents',
      props: { page: 2 },
      meta: {
        requiresAuth: true
      },
      component: Adherents
    }, {
      path: '/adherents/new',
      name: 'NewAdherent',
      props: { page: 2 },
      meta: {
        // requiresAuth: true
      },
      component: AdherentForm
    }, {
      path: '/adherents/:id(\\d+)/edit',
      name: 'EditAdherent',
      props: (route) => ({
        page: 2
      }),
      meta: {
        requiresAuth: true
      },
      component: AdherentForm
    }, {
      path: '/adherents/:id(\\d+)',
      name: 'ShowAdherent',
      props: {
        page: 2
      },
      meta: {
        requiresAuth: true
      },
      component: AdherentFiche
    },
    {
      path: '/news',
      name: 'News',
      props: { page: 3 },
      meta: {
        requiresAuth: true
      },
      component: News
    },
    {
      path: '/news/:id(\\d+)',
      name: 'EditNews',
      props: { page: 3 },
      meta: {
        requiresAuth: true
      },
      component: NewsForm
    }, {
      path: '/news/new',
      name: 'NewNews',
      props: { page: 3 },
      meta: {
        requiresAuth: true
      },
      component: NewsForm
    },
    {
      path: '/courts',
      name: 'Courts',
      props: { page: 4 },
      meta: {
        requiresAuth: true
      },
      component: Courts
    }, {
      path: '/courts/new',
      name: 'NewCourt',
      props: { page: 4 },
      meta: {
        requiresAuth: true
      },
      component: CourtForm
    }, {
      path: '/courts/:id(\\d+)',
      name: 'ShowCourt',
      props: (route) => ({ page: 4,
        court: {
          'id': route.params.id
        }
      }),
      meta: {
        requiresAuth: true
      },
      component: CourtFiche
    }, {
      path: '/courts/:id(\\d+)/edit',
      name: 'EditCourt',
      props: (route) => ({ page: 4,
        court: {
          'id': route.params.id
        }
      }),
      meta: {
        requiresAuth: true
      },
      component: CourtForm
    }, {
      path: '/reservations',
      name: 'Reservations',
      props: { page: 5 },
      meta: {
        requiresAuth: true
      },
      component: Reservations
    }, {
      path: '/reservations/new',
      name: 'NewReservation',
      props: { page: 5 },
      meta: {
        requiresAuth: true
      },
      component: ReservationForm
    }, {
      path: '/reservations/:id(\\d+)',
      name: 'ShowReservation',
      props: (route) => ({ page: 5,
        reservation: {
          'id': route.params.id
        }
      }),
      meta: {
        requiresAuth: true
      },
      component: ReservationFiche
    },
    {
      path: '/config',
      name: 'Configuration',
      props: { page: 6 },
      meta: {
        requiresAuth: true
      },
      component: Config
    },
    {
      path: '/404',
      name: 'BadGateway',
      props: { page: 999 },
      component: BadGateway
    },
    {
      path: '/login',
      name: 'Login',
      props: { page: 998 },
      component: Login
    },
    {
      path: '/passwordreset',
      name: 'PasswordReset',
      props: { page: 997 },
      component: PasswordReset
    },
    {
      path: '*',
      props: { page: 999 },
      redirect: '/404'
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.loggedIn) {
      next()
      return
    }
    next('/login')
  } else {
    next()
  }
})

export default router
