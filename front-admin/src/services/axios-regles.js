import axios from 'axios'
import setup from './interceptor'

const instance = axios.create({
  baseURL: process.env.REGLES_SERVER
})
setup(instance)

export default instance
