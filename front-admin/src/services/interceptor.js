import store from '../store/index'

export default function setup (axios) {
  axios.interceptors.request.use(config => {
    const token = store.state.moduleAuth.access_token

    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
  },
  error => {
    Promise.reject(error)
  })
  // TODO : setup interceptor for response https://medium.com/swlh/handling-access-and-refresh-tokens-using-axios-interceptors-3970b601a5da
}
