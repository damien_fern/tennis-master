import axios from 'axios'

const instance = axios.create({
  baseURL: process.env.AUTH_SERVER,
  auth: {
    username: 'adminweb',
    password: 'adminweb-pwd'
  }
})
export default instance
