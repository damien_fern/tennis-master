/**
 * Mocking client-server processing
 */
import axios from '../services/axios-user'
// import _users from './mocks/users'

export default {
  getUsers (cb) {
    axios
      .get('/users')
      .then((res) => {
        cb(res.data)
      })
  },
  getUser (id, cb) {
    axios
      .get('/users/' + id)
      .then((res) => {
        if (res.data.classement === null) {
          res.data.classement = {
            idClassement: null
          }
        }
        cb(res.data)
      })
  },
  deleteUser (id, cb) {
    axios
      .delete('/users/' + id)
      .then((res) => {
        cb(res.data)
      })
  },
  addUser (data, cb) {
    let copyData = {...data}
    data.roles = [copyData.role]
    console.log(data)
    axios
      .post('/users', data)
      .then((res) => {
        cb(res.data)
      })
  },
  updateUser (data, cb) {
    let copyData = {...data}
    data.roles = [copyData.role]
    axios
      .put('/users/updateUser/' + data.idUtilisateur, data)
      .then((res) => {
        cb(res.data)
      })
  },
  getClassements (cb) {
    axios
      .get('/rankings')
      .then((res) => {
        cb(res.data)
      })
  },
  getRoles (cb) {
    axios
      .get('/roles')
      .then((res) => {
        cb(res.data)
      })
  }
}
