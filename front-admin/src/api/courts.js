/**
 * Mocking client-server processing
 */
import axios from '../services/axios-court'

export default {
  getCourts (cb) {
    axios
      .get('/court')
      .then((res) => {
        cb(res.data)
      })
  },
  getTypeCourts (cb) {
    axios
      .get('/courtKind')
      .then((res) => {
        cb(res.data)
      })
  },
  getCourt (id, cb) {
    axios
      .get('/court/' + id)
      .then((res) => {
        if (res.data.classement === null) {
          res.data.classement = {
            idClassement: null
          }
        }
        cb(res.data)
      })
  },
  deleteCourt (id, cb) {
    axios
      .delete('/court/' + id)
      .then((res) => {
        cb(res.data)
      })
  },
  addCourt (data, cb) {
    axios
      .post('/court', data)
      .then((res) => {
        cb(res.data)
      })
  },
  updateCourt (data, cb) {
    axios
      .put('/court/' + data.idCourt, data)
      .then((res) => {
        cb(res.data)
      })
  }
}
