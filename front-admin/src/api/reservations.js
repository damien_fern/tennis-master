/**
 * Mocking client-server processing
 */
import axios from '../services/axios-reservations'

export default {
  getReservations (cb) {
    axios
      .get('/reservations')
      .then((res) => {
        cb(res.data)
      })
  },
  getReservation (idReservation, cb) {
    axios
      .get('/reservations/' + idReservation)
      .then((res) => {
        cb(res.data)
      })
  },
  getReservationsDispoByCourt (idCourt, dateReservation, cb) {
    axios
      .get('/reservations/planning', {
        params: {
          idCourt: idCourt,
          dateReservation: dateReservation
        }
      })
      .then((res) => {
        cb(res.data[0].reservationVueList)
      })
  },
  addReservation (data, cb) {
    axios
      .post('/reservations', data)
      .then((res) => {
        cb(res.data)
      })
  },
  validerReservation (id, cb) {
    axios
      .put('/reservations/' + id, {
        valide: true
      })
      .then((res) => {
        cb(res.data)
      })
  },
  deleteReservation (id, cb) {
    axios
      .delete('/reservations/' + id)
      .then((res) => {
        cb(res.data)
      })
  }
}
