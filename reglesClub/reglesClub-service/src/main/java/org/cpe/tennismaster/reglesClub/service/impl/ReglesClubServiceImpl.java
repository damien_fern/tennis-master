package org.cpe.tennismaster.reglesClub.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.cpe.tennismaster.common.exception.NotFoundException;
import org.cpe.tennismaster.common.utils.UtilsTools;
import org.cpe.tennismaster.reglesClub.common.dto.ReglesClubDto;
import org.cpe.tennismaster.reglesClub.common.vue.ReglesClubVue;
import org.cpe.tennismaster.reglesClub.model.ReglesClub;
import org.cpe.tennismaster.reglesClub.repository.ReglesClubRepository;
import org.cpe.tennismaster.reglesClub.service.ReglesClubService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class ReglesClubServiceImpl implements ReglesClubService {

    private final ReglesClubRepository reglesClubRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    public ReglesClubServiceImpl(ReglesClubRepository reglesClubRepository) {
        this.reglesClubRepository = reglesClubRepository;
    }

    private ReglesClub getReglesClubModel() throws NotFoundException {
        List<ReglesClub> reglesClubList =  this.reglesClubRepository.findAllBy();
        if(reglesClubList.isEmpty()) {
            throw new NotFoundException("Pas de règle club en base de données");
        }
        return reglesClubList.get(0);
    }

    public ReglesClubVue getReglesClub() {
        ReglesClub reglesClub = getReglesClubModel();
        return modelMapper.map(reglesClub, ReglesClubVue.class);
    }

    public ReglesClubVue updateReglesClub(ReglesClubDto reglesClubDto) throws NotFoundException {
        if (reglesClubDto == null) {
            throw new NotFoundException("Reservation club");
        }

        ReglesClub existingReglesClub = getReglesClubModel();

        ReglesClub reglesClubModel = modelMapper.map(reglesClubDto, ReglesClub.class);
        UtilsTools.copyNonNullProperties(reglesClubModel, existingReglesClub);
        reglesClubModel = reglesClubRepository.save(existingReglesClub);

        return modelMapper.map(reglesClubModel, ReglesClubVue.class);
    }
}
