package org.cpe.tennismaster.notification.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;
import java.util.Date;

@RedisHash(value = "HandleTicketError")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HandleTicketErrorRedis {

    @Id
    private Long id;

    @Indexed
    private String idAVerifier;

    private Date dateAjout;

}
