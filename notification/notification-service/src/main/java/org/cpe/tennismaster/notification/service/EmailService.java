package org.cpe.tennismaster.notification.service;

import freemarker.template.TemplateException;
import org.cpe.tennismaster.notification.common.model.MailDto;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Service
public interface EmailService {

    void sendMail(MailDto mailDto, String template) throws TemplateException, IOException, MessagingException;

}
