package org.cpe.tennismaster.notification.service;

import org.cpe.tennismaster.common.configuration.activeMQ.GeneralActiveMQOperation;
import org.cpe.tennismaster.common.model.Letter;
import org.cpe.tennismaster.common.model.ResponseView;
import org.cpe.tennismaster.notification.common.model.MailDto;
import org.cpe.tennismaster.notification.common.model.NotificationDto;
import org.cpe.tennismaster.notification.common.model.PushDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class NotificationActiveMQOperation extends GeneralActiveMQOperation {

    private final ModelMapper modelMapper;

    private final NotificationService notificationService;

    @Autowired
    public NotificationActiveMQOperation(ModelMapper modelMapper, @Lazy NotificationService notificationService) {
        this.modelMapper = modelMapper;
        this.notificationService = notificationService;
    }

    @Override
    protected void receiveMessage(Letter letter) {
        ResponseView<NotificationDto> responseView = letter.getResponse();
        switch(letter.getIntentAction()){
            case SEND_MAIL_NOTIFICATION:
                MailDto mailDto = modelMapper.map(responseView.getData(), MailDto.class);
                this.notificationService.envoi(mailDto);
                break;
            case SEND_PUSH_NOTIFICATION:
                PushDto pushDto = modelMapper.map(responseView.getData(), PushDto.class);
                this.notificationService.envoi(pushDto);
                break;
        }
    }

}
