package org.cpe.tennismaster.notification.repository;

import org.cpe.tennismaster.notification.model.EtatEnvoi;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EtatEnvoiRepository extends MongoRepository<EtatEnvoi, String> {



}
