package org.cpe.tennismaster.notification.common.model;

public enum NotificationType {

    PUSH("PUSH"),
    MAIL("MAIL");

    private String notification;

    NotificationType(String notification) {
        this.notification = notification;
    }

    public String getNotification() {
        return this.notification;
    }

}
