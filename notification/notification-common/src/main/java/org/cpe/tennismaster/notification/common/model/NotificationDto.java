package org.cpe.tennismaster.notification.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@AllArgsConstructor
@ToString
public class NotificationDto implements Serializable {

    private NotificationType notificationType;

    private IntentNotification action;

    private String from;

    public NotificationDto(){}

}
