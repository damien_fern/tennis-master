package org.cpe.tennismaster.notification.common.exception;

import org.cpe.tennismaster.common.exception.GeneralException;
import org.springframework.http.HttpStatus;

public class PushNotificationsNotSentException extends GeneralException {

    public PushNotificationsNotSentException(String notifications){
        super(notifications);
    }

    @Override
    protected HttpStatus defineStatusHttp() {
        return HttpStatus.CONFLICT;
    }

    @Override
    protected String defineErrorMessage(String variable) {
        return "Certaines notifications push n'ont pas été envoyées : \n" + variable;
    }

}
